﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class SpecialDiscountsDetail
    {

        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void createForm(string form, string execid, string docentry, int row,string approved, string cardCode)
        {
            Global.CreateForm(form);
            SAPbouiCOM.GridColumn oColumn;
            SAPbouiCOM.ComboBoxColumn oCGC;
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                coForm.Freeze(true);
                BindUserDataSources();
                oGrid = coForm.Items.Item("grData").Specific;

                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 34, '" + execid + "', '" + docentry + "'");

                oColumn = oGrid.Columns.Item("Tipo Valor");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;

                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Valor");
                oCGC.ValidValues.Add("I", "Importe");
                oCGC.ValidValues.Add("P", "Porcentaje");
                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;


                oColumn = oGrid.Columns.Item("Marca");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;

                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Marca");

                oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 49");
                oRecSet.MoveFirst();

                for (int i = 0; i < oRecSet.RecordCount; i++)
                {
                    oCGC.ValidValues.Add(oRecSet.Fields.Item("Code").Value, oRecSet.Fields.Item("Name").Value);
                    oRecSet.MoveNext();
                }
               
                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                oGrid.Columns.Item("ExecId").Visible = false;
                oGrid.Columns.Item("Id").Visible = false;
                oGrid.Columns.Item("DocEntry").Visible = false;

                oGrid.Columns.Item("Factura").Editable = false;
                oGrid.Columns.Item("Codigo Descuento").Editable = false;
                oGrid.Columns.Item("Descripcion").Editable = false;
                oGrid.Columns.Item("Marca").Editable = false;
                oGrid.Columns.Item("SubTotal").Editable = false;
                oGrid.AutoResizeColumns();

                coForm.DataSources.UserDataSources.Item("dsRow").Value = (row).ToString();
                coForm.DataSources.UserDataSources.Item("dsCardCode").Value = cardCode;
                if (approved == "Y")
                {
                    oGrid.Item.Enabled = false;
                }
  

                coForm.Freeze(false);
            }
            catch (Exception ex)
            {
                Global.SetMessage("createForm. " + ex.Message, Global.MsgType.Error);
            }

        }


        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.EditText loText;
                SAPbouiCOM.UserDataSource loDS;
                SAPbouiCOM.DataTable oDataTable;

                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    loDS = coForm.DataSources.UserDataSources.Add("dsRow", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtRow").Specific;
                    loText.DataBind.SetBound(true, "", "dsRow");

                    loDS = coForm.DataSources.UserDataSources.Add("dsCardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);


                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }
        }

        public void UpdateDetail()
        {
            SAPbouiCOM.Form auxForm;
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
             
                SAPbouiCOM.Matrix auxMatrix;

                String ValType, ExecId, Id;
                double Value;
                int row;
                SAPbouiCOM.EditText oEdit;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                row = Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value);


                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    ExecId = oGrid.DataTable.GetValue("ExecId", i).ToString();
                    Id = oGrid.DataTable.GetValue("Id", i).ToString();
                    ValType = oGrid.DataTable.GetValue("Tipo Valor", i).ToString();
                    Value = oGrid.DataTable.GetValue("Valor", i);

                    oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 35, '"  + ExecId + "','" + Id +"','" + ValType + "','" + Value.ToString() +"'");
                    
                }

                Global.SetMessage("Detalle actualizado correctamente.", Global.MsgType.Success);

                ExecId = oGrid.DataTable.GetValue("ExecId", 0).ToString();
                Id = oGrid.DataTable.GetValue("DocEntry", 0).ToString();

                oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 36, '" + ExecId + "','" + Id + "'");

                oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 37, '" + ExecId + "','" + Id + "'");
                oRecSet.MoveFirst();

                auxForm = Global.SBOApp.Forms.Item("ADESD");
                auxForm.Freeze(true);


                auxMatrix = auxForm.Items.Item("0_U_G").Specific;

                oEdit = auxMatrix.Columns.Item("C_0_8").Cells.Item(row).Specific;
                oEdit.Value = oRecSet.Fields.Item("Prc").Value.ToString();

                oEdit = auxMatrix.Columns.Item("C_0_9").Cells.Item(row).Specific;
                oEdit.Value = oRecSet.Fields.Item("SubTotalDescuento").Value.ToString();

                auxMatrix.FlushToDataSource();

                auxForm.Refresh();
           

                auxForm.Freeze(false);
                coForm.Close();

            }
            catch (Exception e)
            {
                auxForm = Global.SBOApp.Forms.Item("ADESD");
                auxForm.Freeze(false);

                Global.SetMessage("UpdateDetail. " + e.Message, Global.MsgType.Error);
            }
        }

        public bool ValidatePercents()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                double value;
                String tipovalor;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                for (int i = 0; i < oGrid.Rows.Count; i++)
                {
                    value = oGrid.DataTable.GetValue("Valor", i);
                    tipovalor = oGrid.DataTable.GetValue("Tipo Valor", i);
                    if(tipovalor == "P")
                    {
                        oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 87, '" + coForm.DataSources.UserDataSources.Item("dsCardCode").Value + "','" + value.ToString() + "'");
                        if(oRecSet.RecordCount > 0)
                        {
                            Global.SetMessage("Alguno de los descuentos supera el limite de descuento",Global.MsgType.Warning);
                            return false;
                        }
                    }
                }

                return true;


            }
            catch (Exception e)
            {
                Global.SetMessage("ValidatePercents. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }

    }
}
