﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
namespace AdvancedDiscounts
{
    class SpecialDiscounts
    {

        private SAPbouiCOM.Form coForm;


        public void createForm(string form)
        {
            Global.CreateForm(form);
            EditText oEdit;
            Matrix oMatrix;

            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                coForm.Freeze(true);

                AddChooseFromList();

                oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 33");

                oEdit = coForm.Items.Item("1_U_E").Specific;
                oEdit.Value = oRecSet.Fields.Item(0).Value.ToString();

                oEdit = coForm.Items.Item("0_U_E").Specific;
                oEdit.Value = oRecSet.Fields.Item(0).Value.ToString();

                oEdit = coForm.Items.Item("30").Specific;
                oEdit.ChooseFromListUID = "CFL_5";
                oEdit.ChooseFromListAlias = "DocEntry";

                oMatrix = coForm.Items.Item("0_U_G").Specific;


                coForm.Freeze(false);
            }
            catch (Exception)
            {
                coForm.Freeze(false);
            }
        }
  
        

        public void LoadData()
        {
            try
            {
                Matrix oMatrix;
                EditText oEdit, oEdit2;
                String CardCode, StartDate, EndDate, ExecId;
                double discTotal = 0;

                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                coForm.Freeze(true);

                ExecId = DateTime.Now.ToString("yyyyMMdhhmmss");
                oEdit = coForm.Items.Item("edtExecId").Specific;
                oEdit.Value = ExecId;


                oEdit = coForm.Items.Item("20_U_E").Specific;
                CardCode = oEdit.Value;
                oEdit = coForm.Items.Item("24_U_E").Specific;
                StartDate = oEdit.Value;
                oEdit = coForm.Items.Item("25_U_E").Specific;
                EndDate = oEdit.Value;

                oMatrix = coForm.Items.Item("0_U_G").Specific;
                oMatrix.Clear();

                oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 19, '" + CardCode + "', '" + StartDate + "','" + EndDate +"','" + ExecId + "'" );
                oRecSet.MoveFirst();

                oMatrix.Columns.Item("C_0_4").Width = 18;
                

                for (int i=1; i<= oRecSet.RecordCount; i++)
                {
                    oMatrix.AddRow();


                    oEdit2 = coForm.Items.Item("0_U_E").Specific;

                    oEdit = oMatrix.Columns.Item("C_0_1").Cells.Item(i).Specific;
                    oEdit.Value = oEdit2.Value;

                    oEdit = oMatrix.Columns.Item("C_0_2").Cells.Item(i).Specific;
                    oEdit.Value = i.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_3").Cells.Item(i).Specific;
                    oEdit.Value = i.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_4").Cells.Item(i).Specific;
                    oEdit.Value =   oRecSet.Fields.Item("DocEntry").Value.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_5").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("DocNum").Value.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_6").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("SubTotal").Value.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_8").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("Prc").Value.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_9").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("SubTotalDescuento").Value.ToString();

                    discTotal +=  oRecSet.Fields.Item("SubTotalDescuento").Value;

                    oRecSet.MoveNext();
                }

                oRecSet.DoQuery("B1S_AdvancedDiscounts 50, '" + CardCode + "','"+ ExecId + "'");
                oRecSet.MoveFirst();

                coForm.DataSources.DBDataSources.Item("@B1S_OESD").SetValue("U_DestNum", 0, oRecSet.Fields.Item("DocNum").Value);
                coForm.DataSources.DBDataSources.Item("@B1S_OESD").SetValue("U_DestEntry", 0, oRecSet.Fields.Item("DocEntry").Value);
     
                coForm.Freeze(false);
            }
            catch (Exception ex)
            {
                coForm.Freeze(false);
                Global.SetMessage("Special Discount. LoadData. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void SetDatos( string stChose)
        {
            try
            {
                coForm = Global.SBOApp.Forms.Item("ADESD");
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                EditText oEdit, oEdit2;
                StaticText oStatic;

                switch (stChose)
                {
                    case "SN":

                        oEdit = coForm.Items.Item("20_U_E").Specific;
                        oRecSet.DoQuery("Select CardName From OCRD Where CardCode = '" + oEdit.Value +"'");
                        
                        oEdit2 = coForm.Items.Item("21_U_E").Specific;
                        oEdit2.Value = oRecSet.Fields.Item(0).Value;
                        break;

                }

                if (coForm.Mode == BoFormMode.fm_ADD_MODE)
                {
                    coForm.Items.Item("26").Enabled = true;
                    coForm.Items.Item("btDel").Enabled = true;
                    oStatic = coForm.Items.Item("lblCancel").Specific;
                    oStatic.Caption = "";
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetDatos. " + e.Message, Global.MsgType.Error);
            }
        }

        public void EnableFindField()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                if (coForm.Mode == BoFormMode.fm_FIND_MODE)
                {
                    coForm.Items.Item("1_U_E").Enabled = true;
                }
                else
                {
                    coForm.Items.Item("1_U_E").Enabled = false;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetDatos. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SetMatrixData(string stChose, int row, string column, string value, string description)
        {
            try


            {
                coForm = Global.SBOApp.Forms.Item("ADESD");
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                switch (stChose)
                {
                    case "INV":

                        coForm.DataSources.DBDataSources.Item("@B1S_OESD").SetValue("U_DestNum", 0, description);
                        coForm.DataSources.DBDataSources.Item("@B1S_OESD").SetValue("U_DestEntry", 0, value);

                        break;

                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetData. " + e.Message, Global.MsgType.Error);   
            }
        }


        public void AddChooseFromList()
        {
            try
            {
                ChooseFromListCollection oCFLs;
                Conditions oCons;
                Condition oCon;
                ChooseFromList oCFL;
                ChooseFromListCreationParams oCFLCreationParams;

                oCFLs = coForm.ChooseFromLists;
                oCFLCreationParams = Global.SBOApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams);


                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "13";
                oCFLCreationParams.UniqueID = "CFL_5";
             
                oCFL = oCFLs.Add(oCFLCreationParams);

                oCons = oCFL.GetConditions();

                oCon = oCons.Add();
                oCon.Alias = "DocStatus";
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCon.CondVal = "O";

                oCFL.SetConditions(oCons);

            }
            catch (Exception ex)
            {
                coForm.Freeze(false);
                Global.SetMessage("AddChooseFrmList. " + ex.Message, Global.MsgType.Error);
            }
        }


        public void CallDetailForm(int row, out String docentry, out String execid, out String approved, out String cardCode)
        {
            try

            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                EditText oEdit;
                CheckBox oCheckBox;

                Matrix oMatrix;         
                SpecialDiscountsDetail oSpecialLine = new SpecialDiscountsDetail();

                oMatrix = coForm.Items.Item("0_U_G").Specific;
                oEdit = oMatrix.Columns.Item("C_0_4").Cells.Item(row).Specific;
                docentry = oEdit.Value ;

                oEdit = coForm.Items.Item("edtExecId").Specific;
                execid = oEdit.Value;

                oEdit = coForm.Items.Item("20_U_E").Specific;
                cardCode = oEdit.Value;

                oCheckBox = coForm.Items.Item("24").Specific;
                if (oCheckBox.Checked)
                {
                    approved = "Y";
                }else
                {
                    approved = "N";
                }
             
            }
            catch (Exception ex)
            {
                execid = "";
                docentry = "";
                approved = "N";
                cardCode =  "";
                Global.SetMessage("CallDetailForm. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void EnableCheckButtons()
        {
            try
            {
                SAPbouiCOM.CheckBox oCheckBox, oCheckBox2, oCheckBoxF;
                SAPbouiCOM.StaticText staticText;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBox = coForm.Items.Item("24").Specific;
                oCheckBox2 = coForm.Items.Item("25").Specific;
                oCheckBoxF = coForm.Items.Item("36").Specific;

                if ((Global.GetProfile() == "MAN" || Global.GetProfile() == "AUT"))
                {
                    if (oCheckBox.Checked && !oCheckBoxF.Checked && !oCheckBox2.Checked)
                    {
                        coForm.Items.Item("24").Enabled = false;
                        coForm.Items.Item("25").Enabled = true;
                        coForm.Items.Item("btDel").Enabled = false;
                        coForm.Items.Item("0_U_G").Enabled = false;
                        coForm.Items.Item("36").Enabled = false;
                        if (oCheckBox.Checked && !oCheckBoxF.Checked)
                        {
                            if(Global.GetUserAUT2(coForm.DataSources.DBDataSources.Item("@B1S_OESD").GetValue("U_CardCode", 0)) == Global.oCompany.UserName)
                            {
                                oCheckBoxF.Item.Enabled = true;
                            }
                            else
                            {
                                oCheckBoxF.Item.Enabled = false;
                            }
                           
                        }
                    }

                    else if (oCheckBox2.Checked || oCheckBoxF.Checked)
                    {
                        coForm.Items.Item("24").Enabled = false;
                        coForm.Items.Item("25").Enabled = false;
                        coForm.Items.Item("btDel").Enabled = false;
                        coForm.Items.Item("0_U_G").Enabled = false;
                        coForm.Items.Item("36").Enabled = false;
                    }
                else
                {
                    if (Global.GetUserBP(coForm.DataSources.DBDataSources.Item("@B1S_OESD").GetValue("U_CardCode", 0)) == "Y")
                    {
                        coForm.Items.Item("24").Enabled = true;
                        coForm.Items.Item("25").Enabled = true;
                        coForm.Items.Item("btDel").Enabled = true;
                        coForm.Items.Item("0_U_G").Enabled = true;
                        coForm.Items.Item("36").Enabled = false;
                    }
                    else
                    {
                        coForm.Items.Item("24").Enabled = false;
                        coForm.Items.Item("25").Enabled = false;
                        coForm.Items.Item("36").Enabled = false;
                        coForm.Items.Item("0_U_G").Enabled = true;

                        if (oCheckBox.Checked || oCheckBox2.Checked)
                        {
                            coForm.Items.Item("0_U_G").Enabled = false;
                            coForm.Items.Item("btDel").Enabled = true;
                        }
                    }
                }
                }
                else
                {
                    coForm.Items.Item("24").Enabled = false;
                    coForm.Items.Item("25").Enabled = false;
                    coForm.Items.Item("36").Enabled = false;
                    coForm.Items.Item("0_U_G").Enabled = true;
                    if (oCheckBox.Checked || oCheckBox2.Checked)
                    {
                        coForm.Items.Item("0_U_G").Enabled = false;
                        coForm.Items.Item("btDel").Enabled = true;
                    }

                }

                coForm.Items.Item("26").Enabled = false;
                staticText = coForm.Items.Item("lblCancel").Specific;
                if (coForm.DataSources.DBDataSources.Item("@B1S_OESD").GetValue("Canceled",0).ToString() == "Y")
                {
                    staticText.Caption = "Cancelado";
                }
                else
                {
                    staticText.Caption = "";
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("EnableCheckButtons. " + ex.Message, Global.MsgType.Error);
            }
        }

        public bool CheckData()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                EditText oEdit;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                if (coForm.Mode == BoFormMode.fm_ADD_MODE)
                {
                    oMatrix = coForm.Items.Item("0_U_G").Specific;
                    if (oMatrix.RowCount > 0)
                    {
                        oEdit = coForm.Items.Item("edtExecId").Specific;
                        oRecSet.DoQuery($"B1S_AdvancedDiscounts 66, '{oEdit.Value.ToString()}'");
                        oRecSet.MoveFirst();

                        if (oRecSet.RecordCount > 0)
                        {
                            Global.SetMessage($"No se ha definido codigo de impuesto o cuenta contable para Descuento: {oRecSet.Fields.Item("DiscCode").Value.ToString()} Marca: {oRecSet.Fields.Item("Name").Value.ToString()}", Global.MsgType.Error);
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        Global.SetMessage("No hay registros. ", Global.MsgType.Warning);
                        return false;
                    }
                    
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("EnableCheckButtons. " + ex.Message, Global.MsgType.Error);
                return false;
            }
        }


        public void DeleteRow()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                SAPbouiCOM.EditText oEdit;
                SAPbobsCOM.Recordset oRecSet;
                String ExecId, docentry;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oMatrix = coForm.Items.Item("0_U_G").Specific;
                oRecSet = Global.Recordset();

                oEdit = coForm.Items.Item("edtExecId").Specific;
                ExecId = oEdit.Value;

                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    if (oMatrix.IsRowSelected(i))
                    {

                        oEdit = oMatrix.Columns.Item("C_0_4").Cells.Item(i).Specific;
                        docentry = oEdit.Value;
                        oMatrix.DeleteRow(i);

                        oRecSet.DoQuery("B1S_AdvancedDiscounts 52, '" + ExecId + "','" + docentry + "'" );
                    }
                }

                oMatrix.FlushToDataSource();
                if (coForm.Mode != BoFormMode.fm_ADD_MODE)
                {
                    coForm.Mode = BoFormMode.fm_UPDATE_MODE;
                }
             

            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }


        public void SendMessaage()
        {
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            DBDataSource oDatatable;
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            oDatatable = coForm.DataSources.DBDataSources.Item("@B1S_OESD");
            string stDocEntry = oDatatable.GetValue("DocEntry", 0);

            SAPbobsCOM.CompanyService oComService;
            SAPbobsCOM.MessagesService oMsgService;
            SAPbobsCOM.Message oMessage;

            oComService = Global.oCompany.GetCompanyService();
            oMsgService = (SAPbobsCOM.MessagesService)oComService.GetBusinessService(SAPbobsCOM.ServiceTypes.MessagesService);
            oMessage = (SAPbobsCOM.Message)oMsgService.GetDataInterface(SAPbobsCOM.MessagesServiceDataInterfaces.msdiMessage);
            oMessage.User = 1;     // existing user with email set
            oMessage.Subject = "Nuevo descuento tercer tipo pendiente de aprobacion";
            oMessage.Text = "Documento: " + stDocEntry + "  Fecha: " + System.DateTime.Now.ToString();


            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 64");
            oRecSet.MoveFirst();
            SAPbobsCOM.RecipientCollection cEmailAddress = oMessage.RecipientCollection;

            for (int i=0;i<= oRecSet.RecordCount -1; i++)
            {
                cEmailAddress.Add();
                cEmailAddress.Item(i).SendInternal = SAPbobsCOM.BoYesNoEnum.tYES;
                cEmailAddress.Item(i).UserCode = oRecSet.Fields.Item("USER_CODE").Value.ToString();
                oRecSet.MoveNext();
            }

            SAPbobsCOM.MessageHeader oMsgHead = oMsgService.SendMessage(oMessage);

        }

        public void SetApprovalUser()
        {
            try
            {

                SAPbouiCOM.CheckBox oCheckBox;
                SAPbouiCOM.EditText oEdit;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBox = coForm.Items.Item("24").Specific;
                oEdit = coForm.Items.Item("27_U_E").Specific;

                if (oCheckBox.Checked)
                {
                    oEdit.Value = Global.oCompany.UserName;
                    coForm.Items.Item("btDel").Enabled = false;
                }


                if (!oCheckBox.Checked)
                {
                    oEdit.Value = "";
                    coForm.Items.Item("btDel").Enabled = true;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void SetApprovalUserF()
        {
            try
            {

                SAPbouiCOM.CheckBox oCheckBox2;
                SAPbouiCOM.EditText oEditF;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                
                oCheckBox2 = coForm.Items.Item("36").Specific;           
                oEditF = coForm.Items.Item("37").Specific;

                if (oCheckBox2.Checked)
                {
                    oEditF.Value = Global.oCompany.UserName;
                    coForm.Items.Item("btDel").Enabled = false;
                }


                if (!oCheckBox2.Checked )
                {
                    oEditF.Value = "";
                    coForm.Items.Item("btDel").Enabled = true;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void SetApprovalUserReject()
        {
            try
            {

                SAPbouiCOM.CheckBox oCheckBoxF;
                SAPbouiCOM.EditText oEditF;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBoxF = coForm.Items.Item("25").Specific;
                oEditF = coForm.Items.Item("38").Specific;

                if (oCheckBoxF.Checked)
                {
                    oEditF.Value = Global.oCompany.UserName;
                    coForm.Items.Item("btDel").Enabled = false;
                }


                if (!oCheckBoxF.Checked)
                {
                    oEditF.Value = "";
                    coForm.Items.Item("btDel").Enabled = true;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
            }
        }


        public void SetFormMode()
        {
            try
            {
                SAPbouiCOM.EditText oEdit;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

          
                oEdit = coForm.Items.Item("1_U_E").Specific;
                switch (coForm.Mode)
                {
                    case BoFormMode.fm_FIND_MODE:
                        oEdit.Item.Enabled = true;
                        break;

                    default:
                        oEdit.Item.Enabled = false;
                        break;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetFormMode. " + ex.Message, Global.MsgType.Error);
            }
        }

    }
}
