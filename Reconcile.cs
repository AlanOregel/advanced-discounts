﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using System.Globalization;

namespace AdvancedDiscounts
{
    class Reconcile
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void createForm(string form)
        {
            Global.CreateForm(form);


            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();
            LoadComboType();
            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.EditText loText;
                SAPbouiCOM.UserDataSource loDS;
                SAPbouiCOM.DataTable oDataTable;


                if (coForm.DataSources.UserDataSources.Count == 0)
                {


                    loDS = coForm.DataSources.UserDataSources.Add("dsFrom", SAPbouiCOM.BoDataType.dt_DATE);
                    loText = coForm.Items.Item("edtFrom").Specific;
                    loText.DataBind.SetBound(true, "", "dsFrom");

                    loDS = coForm.DataSources.UserDataSources.Add("dsSN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtCust").Specific;
                    loText.DataBind.SetBound(true, "", "dsSN");

                    loDS = coForm.DataSources.UserDataSources.Add("dsTo", SAPbouiCOM.BoDataType.dt_DATE);
                    loText = coForm.Items.Item("edtTo").Specific;
                    loText.DataBind.SetBound(true, "", "dsTo");

                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    coForm.DataSources.DataTables.Add("ProcData");
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }
        }

        public void LoadComboType()
        {
            try
            {

                SAPbouiCOM.ComboBox oCombo;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                oCombo = coForm.Items.Item("edtType").Specific;

                if (oCombo.ValidValues.Count > 0)
                {
                    do
                    {
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                    }
                    while (oCombo.ValidValues.Count > 0);
                }

                oCombo.ValidValues.Add("TP2", "Inmediato Nota Crédito");
                oCombo.ValidValues.Add("TP3", "Facturas Acumuladas");
                oCombo.ValidValues.Add("TP4", "Opcional");
                oCombo.ValidValues.Add("TP5", "Volumen de  Venta");

                oCombo.Item.DisplayDesc = true;
            }
            catch (Exception e)
            {
                Global.SetMessage("LoadComboType. " + e.Message, Global.MsgType.Error);
            }
        }

        public void LoadData()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                EditText oStarDate, oEndDate;
                string stStrtDate, stEndDate;
                SAPbouiCOM.GridColumn oColumn;
                SAPbouiCOM.ComboBoxColumn oCGC;
                SAPbouiCOM.EditTextColumn oColumns;
                DateTime dtStrtDate = DateTime.Now, dtEndDate = DateTime.Now;

                oGrid = coForm.Items.Item("grData").Specific;
                oStarDate = coForm.Items.Item("edtFrom").Specific;
                oEndDate = coForm.Items.Item("edtTo").Specific;
                SAPbouiCOM.ComboBox oCombo = coForm.Items.Item("edtType").Specific;
                coForm.Freeze(true);


                stStrtDate = oStarDate.Value;
                stEndDate = oEndDate.Value;


                oGrid.DataTable.ExecuteQuery("exec B1S_AdvancedDiscounts 53, '" + stStrtDate + "','" + stEndDate + "', '" + coForm.DataSources.UserDataSources.Item("dsSN").Value + "','" + oCombo.Value.ToString() + "'");

                if (oGrid.Rows.Count > 0)
                {
                    oColumn = oGrid.Columns.Item("Tipo Documento");
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                    oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                    oCGC.ValidValues.Add("PO", "Orden de Compra");
                    oCGC.ValidValues.Add("CM", "Nota de Credito");
                    oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                    oGrid.Columns.Item("DocEntry").Visible = false;
                    oGrid.AutoResizeColumns();

                    oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                    oColumns.Width = 18;
                    oColumns.LinkedObjectType = "13";

                    oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                    oColumns.LinkedObjectType = "2";

                    SetColorEstatus(oGrid, oCombo.Value.ToString());

                }
 
                coForm.Freeze(false);

            }
            catch (Exception e)
            {

                coForm.Freeze(false);
                Global.SetMessage("LoadData. " + e.Message, Global.MsgType.Error);
            }


        }

        public void Reconciliation()
        {
            try
            {
                Global.flag = true;
                SAPbouiCOM.DataTable oDataTable;
                string stStrtDate, stEndDate;
                EditText oStarDate, oEndDate;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbobsCOM.Recordset oRecSet2 = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                SAPbouiCOM.ComboBox oCombo = coForm.Items.Item("edtType").Specific;
                String stType = oCombo.Value.ToString();
                if (oGrid.Rows.Count <= 0)
                {
                    return;
                }

                coForm.Freeze(true);

                oStarDate = coForm.Items.Item("edtFrom").Specific;
                oEndDate = coForm.Items.Item("edtTo").Specific;

                stStrtDate = oStarDate.Value;
                stEndDate = oEndDate.Value;

                oDataTable = coForm.DataSources.DataTables.Item("ProcData");
                oDataTable.ExecuteQuery("exec B1S_AdvancedDiscounts 56, '" + stStrtDate + "','" + stEndDate + "', '" + coForm.DataSources.UserDataSources.Item("dsSN").Value + "','" + stType + "'");

                if (oDataTable.Rows.Count == 0 || oDataTable.Columns.Item("Cliente").Cells.Item(0).Value.ToString() == "")
                {
                    coForm.Freeze(false);
                    return;
                }

                Global.SBOApp.ActivateMenuItem("9459");
                SAPbouiCOM.EditText oEdit;
                SAPbouiCOM.Matrix oMatrix;
                SAPbouiCOM.CheckBox oCheckBox;
                String value, recdoctype, stdiscamount;
                double discamount;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                coForm.Freeze(true);
                oEdit = coForm.Items.Item("120000008").Specific;
                oEdit.Value = oDataTable.Columns.Item("Cliente").Cells.Item(0).Value;
                coForm.Items.Item("120000001").Click();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                coForm.Freeze(true);

                oMatrix = coForm.Items.Item("120000039").Specific;

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    if (oDataTable.GetValue("Estado", i) == "Generado" && oDataTable.GetValue("Tipo Documento", i) == "CM")
                    {

                        for (int j = 1; j <= oMatrix.RowCount; j++)
                        {
                            oEdit = oMatrix.Columns.Item("10000045").Cells.Item(j).Specific;
                            value = oEdit.Value;

                            oEdit = oMatrix.Columns.Item("120000005").Cells.Item(j).Specific;
                            recdoctype = oEdit.Value;

                            if ((int.Parse(value) == oDataTable.GetValue("Documento", i)) && (recdoctype == "RC" || recdoctype == "CN"))
                            {
                                oCheckBox = oMatrix.Columns.Item("120000002").Cells.Item(j).Specific;
                                oCheckBox.Checked = true;

                                oEdit = oMatrix.Columns.Item("120000027").Cells.Item(j).Specific;
                                stdiscamount = oEdit.Value;
                                stdiscamount = stdiscamount.Replace("MXN", "");
                                stdiscamount = stdiscamount.Replace("MXP", "");
                                stdiscamount = stdiscamount.Replace("(", "");
                                stdiscamount = stdiscamount.Replace(")", "");
                                stdiscamount = stdiscamount.Replace("-", "");

                                discamount = Convert.ToDouble(stdiscamount);

                                for (int k = 1; k <= oMatrix.RowCount; k++)
                                {
                                    oEdit = oMatrix.Columns.Item("10000045").Cells.Item(k).Specific;
                                    value = oEdit.Value;

                                    oEdit = oMatrix.Columns.Item("120000005").Cells.Item(k).Specific;
                                    recdoctype = oEdit.Value;

                                    if ((int.Parse(value) == oDataTable.GetValue("Documento Destino", i)) && (recdoctype == "IN" || recdoctype == "RF"))
                                    {
                                        oCheckBox = oMatrix.Columns.Item("120000002").Cells.Item(k).Specific;
                                        oCheckBox.Checked = true;

                                        oEdit = oMatrix.Columns.Item("120000027").Cells.Item(k).Specific;
                                        oEdit.Value = (discamount ).ToString();
                                    }

                                }
                            }

                        }

                    }
                }



                coForm.Items.Item("120000001").Click();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                coForm.Freeze(true);
                coForm.Items.Item("120000001").Click();

                for (int i = 0; i < oDataTable.Rows.Count; i++)
                {
                    oRecSet.DoQuery("exec B1S_AdvancedDiscounts 57, '" + oDataTable.GetValue("DocEntry", i) + "','" + stType + "'");
                    oRecSet.MoveFirst();

                    if (oRecSet.Fields.Item(0).Value == "C")
                    {
                        oRecSet2.DoQuery("exec B1S_AdvancedDiscounts 58, '" + oDataTable.GetValue("DocEntry", i) + "','" + stType + "'");
                        oRecSet2.MoveFirst();
                    }
                }

                coForm = Global.SBOApp.Forms.Item("B1SADREC");
                LoadData();
                coForm.Freeze(false);
                Global.flag = false;
            }
            catch (Exception e)
            {
                Global.flag = false;
                coForm.Freeze(false);
                coForm.Freeze(false);
                coForm.Freeze(false);
                Global.SetMessage("Reconcile. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SetDatos(string stValue, string stChose, string stValue2 = "")
        {
            try
            {
                coForm = Global.SBOApp.Forms.Item("B1SADREC");

                switch (stChose)
                {
                    case "SN":
                        coForm.DataSources.UserDataSources.Item("dsSN").Value = stValue;

                        break;
                    case "WHS":
                        coForm.DataSources.UserDataSources.Item("dsWHS").Value = stValue;
                        break;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetDatos. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SetColorEstatus(Grid oGrid, String type)
        {
            int auxIndex;
            string estatus;
            try
            {
                for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                {
                    auxIndex = oGrid.GetDataTableRowIndex(i);
                    estatus = oGrid.DataTable.GetValue("Estado", auxIndex).ToString();
                    switch (estatus)
                    {
                        case "Generado":
                            switch (type)
                            {
                                case "TP3":
                                case "TP5":
                                    oGrid.CommonSetting.SetCellFontColor((i + 1), 9, 888888);
                                    oGrid.CommonSetting.SetCellFontColor((i + 1), 8, 0);
                                    break;
                                case "TP2":
                                case "TP4":
                                    oGrid.CommonSetting.SetCellFontColor((i + 1), 8, 888888);
                                    break;
                            }
                          

                            break;
                        case "Reconciliado":
                            switch (type)
                            {
                                case "TP3":
                                case "TP5":
                                    oGrid.CommonSetting.SetCellFontColor((i + 1), 9, 16711680);
                                    oGrid.CommonSetting.SetCellFontColor((i + 1), 8, 0);
                                    break;
                                case "TP2":
                                case "TP4":
                                    oGrid.CommonSetting.SetCellFontColor((i + 1), 8, 16711680);
                                    break;
                            }
                                    
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage(("Error asignar Color Estatus: " + ex.Message), Global.MsgType.Error);
            }

        }
    }
}
