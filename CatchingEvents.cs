﻿using System;
using System.Windows.Forms;
using SAPbouiCOM;

namespace AdvancedDiscounts
{
    class CatchingEvents
    {

        public CatchingEvents()
        {
            addMenuItems();
        }

        private void addMenuItems()
        {
            SAPbouiCOM.Form loForm;

            try
            {

                loForm = Global.SBOApp.Forms.GetForm("169", 1);
                loForm.Freeze(true);

                Global.AddMenu("43520", BoMenuType.mt_SEPERATOR, "B1SADDIS", 500, "Descuentos avanzados", "Logo.png");
          
                Global.AddMenu("B1SADDIS", BoMenuType.mt_STRING, "B1SADDIS1", 1, "Descuento Facturas Acumuladas", "");
                Global.AddMenu("B1SADDIS", BoMenuType.mt_STRING, "B1SADDIS5", 2, "Descuento Volumen de Venta", "");
                Global.AddMenu("B1SADDIS", BoMenuType.mt_STRING, "B1SADDIS4", 3, "Monitor", "");
                Global.AddMenu("B1SADDIS", BoMenuType.mt_STRING, "B1SADDIS3", 4, "Reconciliacion", "");

                if (Global.GetProfile()== "MAN")
                {
                Global.AddMenu("B1SADDIS", BoMenuType.mt_POPUP, "B1SADDIS2", 5, "Definiciones", "");
                Global.AddMenu("B1SADDIS2", BoMenuType.mt_STRING, "B1SADDIS21", 6, "Descuentos", "");

                Global.AddMenu("B1SADDIS2", BoMenuType.mt_STRING, "B1SADDIS22", 7, "Lista Inclusion", "");
                Global.AddMenu("B1SADDIS2", BoMenuType.mt_STRING, "B1SADDIS23", 8, "Lista Exclusion", "");
                Global.AddMenu("B1SADDIS2", BoMenuType.mt_STRING, "B1SADDIS24", 9, "Impuesto / Cuenta Contable", "");
                Global.AddMenu("B1SADDIS2", BoMenuType.mt_STRING, "B1SADDIS25", 10, "Codigo de descuento", "");
                Global.AddMenu("B1SADDIS2", BoMenuType.mt_STRING, "B1SADDIS26", 11, "Importacion masiva", "");

                }

                loForm.Freeze(false);
                loForm.Update();

            }
            catch (Exception ex)
            {
                loForm = Global.SBOApp.Forms.GetForm("169", 1);
                loForm.Freeze(false);
                Global.SetMessage("Error al agregar menú: " + ex.Message, Global.MsgType.Error);
            }
        }

        public void SBOApplication_FormDataEvent(ref BusinessObjectInfo BusinnesObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if(BusinnesObjectInfo.FormTypeEx == "ADESD")
                {
                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD & BusinnesObjectInfo.ActionSuccess)
                    {
                        SpecialDiscounts oSpecialDis = new SpecialDiscounts();
                        oSpecialDis.EnableCheckButtons();
                    }

                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD & BusinnesObjectInfo.ActionSuccess)
                    {

                        SpecialDiscounts oSpecialDis = new SpecialDiscounts();
                        oSpecialDis.SendMessaage();
                    }

                }

                if (BusinnesObjectInfo.FormTypeEx == "B1SOSLD")
                {
                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD & BusinnesObjectInfo.ActionSuccess)
                    {
                        SalesDiscount oSalesDis = new SalesDiscount();
                        oSalesDis.EnableCheckButtons();
                    }

                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD & BusinnesObjectInfo.ActionSuccess)
                    {

                        SalesDiscount oSalesDis = new SalesDiscount();
                        oSalesDis.SendMessaage();
                    }

                }


                if (BusinnesObjectInfo.FormTypeEx == "139")
                {
                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD & BusinnesObjectInfo.ActionSuccess)
                    {
                       
                        SalesOrder oSalesOrder = new SalesOrder();
                        oSalesOrder.UpdateDiscountDetail(BusinnesObjectInfo.FormUID);
                        
                    }

                    if (BusinnesObjectInfo.BeforeAction == true & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
                    {
                        SalesOrder oSalesOrder = new SalesOrder();
                    //    oSalesOrder.SetDiscountPercent("");
                    }
                }

                if (BusinnesObjectInfo.FormTypeEx == "133")
                {
                    if (BusinnesObjectInfo.BeforeAction == false & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD & BusinnesObjectInfo.ActionSuccess)
                    {
                        Invoice oInvoice = new Invoice();
                        oInvoice.CreateCMDetail(BusinnesObjectInfo.FormUID);

                    }

                    if (BusinnesObjectInfo.BeforeAction == true & BusinnesObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
                    {
                        SalesOrder oSalesOrder = new SalesOrder();
                        //    oSalesOrder.SetDiscountPercent("");
                    }
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("FormDataEvent" + e.Message, Global.MsgType.Error);
            }
        }

        public void SBOApplication_MenuEvent(ref MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.BeforeAction == true & pVal.MenuUID == "B1SADDIS21")
                {
                    AdvancedDiscounts oAdvDisc = new AdvancedDiscounts();
                    oAdvDisc.createForm("ADDEF");

                }

                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS22")
                {
                    IncludeList oIncludeList = new IncludeList();
                    oIncludeList.createForm("ADDINL");
                }


                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS23")
                {
                    IncludeList oIncludeList = new IncludeList();
                    oIncludeList.createForm("ADDEXL");
                }

                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS1")
                {
                    SpecialDiscounts oSpecialDiscounts = new SpecialDiscounts();
                    oSpecialDiscounts.createForm("ADESD");
                }

                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS24")
                {
                    AccountTax oActTax = new AccountTax();
                    oActTax.createForm("ADTAX");
                }

                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS25")
                {
                    Discounts oDis = new Discounts();
                    oDis.createForm("ADDIS");
                }

                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS3")
                {
                    Reconcile oReconcile = new Reconcile();
                    oReconcile.createForm("B1SADREC");
                }


                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS4")
                {
                    Monitor oMonitor = new Monitor();
                    oMonitor.createForm("B1SADMON");
                }

                if(pVal.BeforeAction & pVal.MenuUID == "B1SADDIS26")
                {
                    Import oImport = new Import();
                    oImport.createForm("B1SADIMPORT");
                }

                if (pVal.BeforeAction & pVal.MenuUID == "B1SADDIS5")
                {
                    SalesDiscount oSalesDiscount = new SalesDiscount();
                    oSalesDiscount.createForm("B1SOSLD");
                }               

            }
            catch (Exception e)
            {
                Global.SetMessage("Menu Event:" + e.Message, Global.MsgType.Error);
            }
        }

        public void SBOApplication_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            try
            {
                BubbleEvent = true;


                if (pVal.BeforeAction == false & pVal.FormTypeEx != "")
                {
                    switch (pVal.FormTypeEx)
                    {
                        case "ADDEF":
                            frmADDEFControllerAfter(FormUID, pVal);
                            break;
                        case "B1CFL":
                            frmB1CFLControllerAfter(FormUID, pVal);
                            break;
                        case "ADDINL":
                            frmADDINLControllerAfter(FormUID, pVal);
                            break;
                        case "ADDEXL":
                            frmADDEXLControllerAfter(FormUID, pVal);
                            break;
                        case "ADESD":
                            frmADESDControllerAfter(FormUID, pVal);
                            break;
                        case "ADESD1":
                            frmADESD1ControllerAfter(FormUID, pVal);
                            break;
                        case "ADTAX":
                            frmADTAXControllerAfter(FormUID, pVal);
                            break;
                        case "ADDIS":
                            frmADDISControllerAfter(FormUID, pVal);
                            break;
                        case "B1SADREC":
                            frmB1SADRECControllerAfter(FormUID, pVal);
                            break;
                        case "B1SADMON":
                            frmB1SADMONControllerAfter(FormUID, pVal);
                            break;
                        case "B1SADIMPORT":
                            frmB1SADIMPORTControllerAfter(FormUID, pVal);
                            break;
                        case "139":
                            frmSalesOrderControllerAfter(FormUID, pVal);
                            break;
                        case "B1SDISDET":
                            frmB1SDISDETControlllerAfter(FormUID, pVal);
                            break;
                        case "B1SOPTDIS":
                            frmB1SOPTDISControlllerAfter(FormUID, pVal);
                            break;
                        case "133":
                            frmInvoiceControllerAfter(FormUID, pVal);
                            break;
                        case "B1SOSLD":
                            frmB1SOSLDControllerAfter(FormUID, pVal);
                            break;
                    }
                }

                if (pVal.BeforeAction == true & pVal.FormTypeEx != "")
                {
                    switch (pVal.FormTypeEx)
                    {
                        case "ADDEF":
                            frmADDEFControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "B1CFL":
                            frmB1CFLControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "ADDINL":
                            frmADDINLControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "ADDEXL":
                            frmADDEXLControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "ADESD":
                            frmADESDControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "ADTAX":
                            frmADTAXControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "ADDIS":
                            frmADDISControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "0":
                            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                            {
                                if (Global.flag)
                                {
                                    Global.SBOApp.SendKeys("{TAB}");
                                    Global.SBOApp.SendKeys("{TAB}");
                                    Global.SBOApp.SendKeys("{TAB}");
                                    Global.SBOApp.SendKeys("{ENTER}");
                                }
                            }
                               
                            break;
                        case "139":
                            frmSalesOrderControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "B1SOSLD":
                            frmB1SOSLDControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                        case "ADESD1":
                            frmADESD1ControllerBefore(FormUID, pVal, out BubbleEvent);
                            break;
                    }
                }

            }
            catch (Exception e)
            {
                BubbleEvent = false;
                Global.SetMessage("ItemEvent: " + e.Message, Global.MsgType.Error);
            }

        }

        public void frmADDEFControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {

            AdvancedDiscounts oAdvDisc;

            string stCode;
            SAPbouiCOM.DataTable oDataTable;
            try
            {
                oAdvDisc = new AdvancedDiscounts();
                ChooseFrmLst oCFL = new ChooseFrmLst();
                string stName;
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btFind":
                                oAdvDisc.LoadData();
                                break;
                            case "btAdd":
                                oAdvDisc.AddRow();
                                break;
                            case "btDel":
                                oAdvDisc.GridRowDelete();
                                break;
                            case "btSave":
                                oAdvDisc.SaveData();
                                break;
                        }
                        break;


                    case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT:
                        switch (pVal.ItemUID)
                        {
                            case "grData":
                                switch (pVal.ColUID)
                                {
                                    case "Tipo":
                                        oAdvDisc.DisableColumns(pVal.Row);
                                        break;
                                    case "Desencadenador":
                                        oAdvDisc.DisableColumnsDocType(pVal.Row);
                                        break;
                                    case "Tipo Valor":
                                        oAdvDisc.DisableColumnsSubTotal(pVal.Row);
                                    break;

                                }
                                break;
                        }
                        break;
                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:

                        SAPbouiCOM.IChooseFromListEvent oCFLEvent;
                        oCFLEvent = (SAPbouiCOM.IChooseFromListEvent)pVal;

                        oDataTable = oCFLEvent.SelectedObjects;
                        switch (oCFLEvent.ChooseFromListUID)
                        {
                            case "CFL_2":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0);
                                    stName = oDataTable.GetValue(1, 0);
                                    oAdvDisc.SetDatos(stCode, "SN", stName);
                                }
                                catch (Exception)
                                {
                                }

                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oAdvDisc = null;
            }

        }
        public void frmADDEFControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            AdvancedDiscounts oAdvDisc;
            try
            {
                oAdvDisc = new AdvancedDiscounts();
                ChooseFrmLst oCFL = new ChooseFrmLst();

                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_VALIDATE:
                        switch (pVal.ItemUID)
                        {
                            case "grData":
                                switch (pVal.ColUID)
                                {
                                    case "Valor":
                                        BubbleEvent = oAdvDisc.ValidatePercent(pVal.Row);
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:


                        switch (pVal.ItemUID)
                        {
                            case "grData":

                                switch (pVal.ColUID)
                                {
                                    case "Codigo asignacion":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "IncCode", pVal.Row.ToString());
                                                break;
                                            case 46:
                                                BubbleEvent = true;
                                                break;
                                            default:
                                                BubbleEvent = false;
                                                break;

                                        }
                                        break;

                                    case "Descuento":
                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "DiscCode2", pVal.Row.ToString());
                                                break;
                                            case 46:
                                                BubbleEvent = true;
                                                break;
                                            default:
                                                BubbleEvent = false;
                                                break;

                                        }
                                        break;

                                    case "Codigo exclusion":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "ExcCode", pVal.Row.ToString());
                                                break;
                                            case 46:
                                                BubbleEvent = true;
                                                break;
                                            default:
                                               
                                                BubbleEvent = false;
                                                break;

                                        }
                                        break;

                                    case "Cuenta":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "Account", pVal.Row.ToString());
                                                break;
                                            case 46:
                                                BubbleEvent = true;
                                                break;
                                            default:
                                                BubbleEvent = false;
                                                break;

                                        }
                                        break;



                                    case "Codigo Direccion":

                                        switch (pVal.CharPressed)
                                        {                                          
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "Address", pVal.Row.ToString(), oAdvDisc.stCardCode);
                                                break;
                                            case 46:
                                                BubbleEvent = true;
                                                break;
                                            default:
                                                BubbleEvent = false;
                                                break;

                                        }
                                        break;

                                    default:
                                        BubbleEvent = true;
                                        break;
                                }

                                break;

                            default:
                                BubbleEvent = true;
                                break;
                        }

                        break;

                    default:
                        BubbleEvent = true;
                        break;

                    case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK:
                        switch (pVal.ItemUID)
                        {
                            case "grData":
                                switch (pVal.ColUID)
                                {
                                    case "RowsHeader":
                                        BubbleEvent = false;
                                        break;
                                    default:

                                        BubbleEvent = true;
                                        break;
                                }
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;

                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oAdvDisc = null;
            }

        }

        public void frmB1CFLControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                ChooseFrmLst oCFL = new ChooseFrmLst();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btSelect":
                                oCFL.SetData();
                                break;
                            case "btFind":
                                oCFL.FilterData();
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:

                        switch (pVal.ItemUID)
                        {
                            case "edtFind":
                                switch (pVal.CharPressed)
                                {
                                    case 9:
                                        oCFL.FilterData();
                                        break;
                                }
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK:
                        switch (pVal.ItemUID)
                        {
                            case "grData":

                                switch (pVal.ColUID)
                                {
                                    case "RowsHeader":
                                        oCFL.SetData();
                                        break;
                                }
                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }
        public void frmB1CFLControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            AdvancedDiscounts oAdvDisc;
            try
            {
                oAdvDisc = new AdvancedDiscounts();
                ChooseFrmLst oCFL = new ChooseFrmLst();

                switch (pVal.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK:
                        switch (pVal.ItemUID)
                        {
                            case "grData":
                                switch (pVal.ColUID)
                                {
                                    case "RowsHeader":
                                        BubbleEvent = false;
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;

                    default:

                        BubbleEvent = true;
                        break;

                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oAdvDisc = null;
            }

        }

        public void frmADDINLControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                IncludeList oIncldeList = new IncludeList();

                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btAdd":
                                oIncldeList.AddRow();
                                break;
                            case "btDel":
                                oIncldeList.DeleteRow();
                                break;

                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }
        public void frmADDINLControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            IncludeList oIncList;
            try
            {
                oIncList = new IncludeList();
                ChooseFrmLst oCFL = new ChooseFrmLst();

                switch (pVal.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                                BubbleEvent = true;
                                oIncList.SetData();
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:


                        switch (pVal.ItemUID)
                        {
                            case "0_U_G":

                                switch (pVal.ColUID)
                                {
                                    case "C_0_3":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "LevelList", pVal.Row.ToString(), oIncList.GetLevel(pVal.Row));
                                                break;
                                            default:
                                                BubbleEvent = false;
                                                break;

                                        }
                                        break;

                                    default:
                                        BubbleEvent = true;
                                        break;
                                }

                                break;

                            default:
                                BubbleEvent = true;
                                break;
                        }

                        break;

                    default:

                        BubbleEvent = true;
                        break;

                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oIncList = null;
            }

        }

        public void frmADDEXLControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                ExcludeList oExcList = new ExcludeList();

                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btAdd":
                                oExcList.AddRow();
                                break;
                            case "btDel":
                                oExcList.DeleteRow();
                                break;

                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }
        public void frmADDEXLControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            ExcludeList oExcList;
            try
            {
                oExcList = new ExcludeList();
                ChooseFrmLst oCFL = new ChooseFrmLst();
                switch (pVal.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                                BubbleEvent = true;
                                oExcList.SetData();
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:


                        switch (pVal.ItemUID)
                        {
                            case "0_U_G":

                                switch (pVal.ColUID)
                                {
                                    case "C_0_3":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "LevelList", pVal.Row.ToString(), oExcList.GetLevel(pVal.Row));
                                                break;
                                            default:
                                                BubbleEvent = true;
                                                break;

                                        }
                                        break;

                                    default:
                                        BubbleEvent = true;
                                        break;
                                }

                                break;

                            default:
                                BubbleEvent = true;
                                break;
                        }

                        break;

                    default:

                        BubbleEvent = true;
                        break;

                }

                // Validations

                switch (pVal.ItemUID)
                {
                    case "0_U_G":
                        switch (pVal.ColUID)
                        {
                            case "C_0_3":
                                switch (pVal.EventType)
                                {
                                    case BoEventTypes.et_VALIDATE:
                                        BubbleEvent = oExcList.ValidateSKU(pVal.Row);
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;


                    default:

                        BubbleEvent = true;
                        break;
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oExcList = null;
            }

        }

        public void frmADESDControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {

            SpecialDiscounts oSpecialDis;

            string stCode;
            SAPbouiCOM.DataTable oDataTable;
            try
            {
                oSpecialDis = new SpecialDiscounts();
                ChooseFrmLst oCFL = new ChooseFrmLst();
                string stName;
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "26":
                                oSpecialDis.LoadData();
                                break;
                            case "btDel":
                                oSpecialDis.DeleteRow();
                                break;
                            case "25":
                                oSpecialDis.SetApprovalUserReject();
                                break;
                            case "24":
                                oSpecialDis.SetApprovalUser();
                                break;
                            case "36":
                                oSpecialDis.SetApprovalUserF();
                                break;
                        }
                        break;
                    case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS:
                        switch (pVal.ItemUID)
                        {
                            case "20_U_E":
                                  oSpecialDis.SetDatos("SN");
                                break;
                        }
             
                        break;

                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:

                        SAPbouiCOM.IChooseFromListEvent oCFLEvent;
                        oCFLEvent = (SAPbouiCOM.IChooseFromListEvent)pVal;

                        oDataTable = oCFLEvent.SelectedObjects;
                        switch (oCFLEvent.ChooseFromListUID)
                        {
                            case "CFL_5":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0).ToString();
                                    stName = oDataTable.GetValue(1, 0).ToString();
                                    oSpecialDis.SetMatrixData("INV",pVal.Row,"C_0_7",stCode, stName);
                                }
                                catch (Exception)
                                {
                                   
                                }

                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_CLICK:

                        switch (pVal.ItemUID)
                        {
                            case "0_U_G":
                                switch (pVal.ColUID)
                                {
                                    case "C_0_5":
                                        String execid, docentry, approved, cardCode;
                                        oSpecialDis.CallDetailForm(pVal.Row,out docentry, out execid, out approved, out cardCode);
                                        SpecialDiscountsDetail oSpecialDisLine = new SpecialDiscountsDetail();

                                        oSpecialDisLine.createForm("ADESD1", execid, docentry, pVal.Row, approved,cardCode);

                                        break;
                                }
                                break;
                            case "1_U_E":
                                oSpecialDis.EnableFindField();
                                break;

                        }

                        break;
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        oSpecialDis.SetFormMode();
                        break;

                }
            }
            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oSpecialDis = null;
            }

        }
        public void frmADESDControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            SpecialDiscounts oSpeDis;
            try
            {
                oSpeDis = new SpecialDiscounts();

                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK:
                        
                        switch (pVal.ItemUID)
                        {
                            case "0_U_G":
                                switch (pVal.ColUID)
                                {
                                    case "#":
                                        BubbleEvent = false;
                                        break;
                                    default:

                                        BubbleEvent = true;
                                        break;
                                }
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;
                    case BoEventTypes.et_KEY_DOWN:
                        switch (pVal.ItemUID)
                        {
                            case "0_U_G":
                                switch (pVal.ColUID)
                                {
                                    case "C_0_8":
                                        BubbleEvent = false;
                                        break;
                                    case "C_0_9":
                                        BubbleEvent = false;
                                        break;
                                    default:

                                        BubbleEvent = true;
                                        break;
                                }
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;

                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":

                                BubbleEvent = oSpeDis.CheckData();
                                break;

                            default:

                                BubbleEvent = true;
                                break;

                        }
                                break;
                    default:
                        BubbleEvent = true;
                        break;
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oSpeDis = null;
            }

        }

        public void frmADESD1ControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                SpecialDiscountsDetail oSpecialLine = new SpecialDiscountsDetail();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btnUpdate":
                                oSpecialLine.UpdateDetail();
                                break;
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }
        public void frmADESD1ControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            SpecialDiscountsDetail oSpeDis;
            try
            {
                oSpeDis = new SpecialDiscountsDetail();

                switch (pVal.EventType)
                {
                    case BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btnUpdate":
                                BubbleEvent = oSpeDis.ValidatePercents();     
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;
                    default:
                        BubbleEvent = true;
                        break;
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oSpeDis = null;
            }

        }

        public void frmADDISControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                Discounts oDiscount = new Discounts();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                                oDiscount.SaveData();
                                break;
                            case "btDel":
                                oDiscount.GridRowDelete();
                                break;
                            case "btAdd":
                                oDiscount.AddRow();
                                break;
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }
        public void frmADDISControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            Discounts oDiscount;
            try
            {
                oDiscount = new Discounts();

                switch (pVal.ItemUID)
                {
                    case "grData":
                        switch (pVal.ColUID)
                        {
                            case "Codigo Descuento":
                                switch (pVal.EventType)
                                {
                                    case BoEventTypes.et_VALIDATE:
                                        BubbleEvent = oDiscount.ValidateDiscCode(pVal.Row);                    
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;


                    default:

                        BubbleEvent = true;
                        break;
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
                oDiscount = null;
            }

        }

        public void frmADTAXControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                AccountTax oActTax = new AccountTax();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                               oActTax.SaveData();
                                break;
                            case "btDel":
                                oActTax.GridRowDelete();
                                break;
                            case "btAdd":
                                oActTax.AddRow();
                                break;
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }
        public void frmADTAXControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

             try
            {
                AccountTax oActTax = new AccountTax();
                ChooseFrmLst oCFL = new ChooseFrmLst();

                switch (pVal.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:


                        switch (pVal.ItemUID)
                        {
                            case "grData":

                                switch (pVal.ColUID)
                                {

                                    case "Cuenta":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "Account", pVal.Row.ToString());
                                                break;
                                            default:
                                                BubbleEvent = true;
                                                break;

                                        }
                                        break;

                                    case "Codigo Impuesto":

                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "TaxCode", pVal.Row.ToString());
                                                break;
                                            default:
                                                BubbleEvent = true;
                                                break;

                                        }
                                        break;
                                    case "Codigo Descuento":
                                        switch (pVal.CharPressed)
                                        {
                                            case 9: //p para asignar
                                                BubbleEvent = false;
                                                oCFL.createForm("B1CFL", pVal.FormTypeEx, "DiscCode", pVal.Row.ToString());
                                                break;
                                            default:
                                                BubbleEvent = true;
                                                break;

                                        }
                                        break;


                                    default:
                                        BubbleEvent = true;
                                        break;
                                }

                                break;

                            default:
                                BubbleEvent = true;
                                break;
                        }

                        break;

                    default:
                        BubbleEvent = true;
                        break;

                    case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK:
                        switch (pVal.ItemUID)
                        {
                            case "grData":
                                switch (pVal.ColUID)
                                {
                                    case "RowsHeader":
                                        BubbleEvent = false;
                                        break;
                                    default:

                                        BubbleEvent = true;
                                        break;
                                }
                                break;

                            default:

                                BubbleEvent = true;
                                break;
                        }

                        break;

                }


                //Validations
                switch (pVal.ItemUID)
                {
                    case "grData":
                        switch (pVal.ColUID)
                        {
                            case "Codigo Descuento":
                                switch (pVal.EventType)
                                {
                                    case BoEventTypes.et_VALIDATE:
                                        if (oActTax.ValidateDuplicated(pVal.Row))
                                        {
                                            BubbleEvent = oActTax.ValidateDiscCode(pVal.Row, pVal.ColUID);
                                        }
                                        else
                                        {
                                            BubbleEvent = false;
                                        }
                                        
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;

                            case "Marca":
                                switch (pVal.EventType)
                                {
                                    case BoEventTypes.et_VALIDATE:          
                                            BubbleEvent = oActTax.ValidateDuplicated(pVal.Row);
                                       break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            case "Codigo Impuesto":
                                switch (pVal.EventType)
                                {
                                    case BoEventTypes.et_VALIDATE:
                                        BubbleEvent = oActTax.ValidateDiscCode(pVal.Row, pVal.ColUID);
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            case "Cuenta":
                                switch (pVal.EventType)
                                {
                                    case BoEventTypes.et_VALIDATE:
                                        BubbleEvent = oActTax.ValidateDiscCode(pVal.Row, pVal.ColUID);
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;


                    default:

                        BubbleEvent = true;
                        break;
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
            finally
            {
               
            }

        }

        public void frmB1SADRECControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                string stCode, stName;
                SAPbouiCOM.DataTable oDataTable;
                Reconcile oReconcile = new Reconcile();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btFind":
                                oReconcile.LoadData();
                                break;
                            case "btRecon":
                                oReconcile.Reconciliation();
                                break;
             
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:

                        SAPbouiCOM.IChooseFromListEvent oCFLEvent;
                        oCFLEvent = (SAPbouiCOM.IChooseFromListEvent)pVal;

                        oDataTable = oCFLEvent.SelectedObjects;
                        switch (oCFLEvent.ChooseFromListUID)
                        {
                            case "CFL_2":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0);
                                    stName = oDataTable.GetValue(1, 0);
                                    oReconcile.SetDatos(stCode, "SN", stName);
                                }
                                catch (Exception)
                                {
                                }

                                break;
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }

        public void frmB1SADMONControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                string stCode, stName;
                SAPbouiCOM.DataTable oDataTable;
                Monitor oMonitor = new Monitor();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btFind":
                                oMonitor.LoadData();
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:

                        SAPbouiCOM.IChooseFromListEvent oCFLEvent;
                        oCFLEvent = (SAPbouiCOM.IChooseFromListEvent)pVal;

                        oDataTable = oCFLEvent.SelectedObjects;
                        switch (oCFLEvent.ChooseFromListUID)
                        {
                            case "CFL_2":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0);
                                    stName = oDataTable.GetValue(1, 0);
                                    oMonitor.SetDatos(stCode, "SN", stName);
                                }
                                catch (Exception)
                                {
                                }

                                break;

                            case "CFL_3":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0);
                                    stName = oDataTable.GetValue(1, 0);
                                    oMonitor.SetDatos(stCode, "SN2", stName);
                                }
                                catch (Exception)
                                {
                                }

                                break;
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }

        public void frmB1SADIMPORTControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                Import oImport = new Import();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btDis":
                            case "btInc":
                            case "btExc":
                            case "btTaxAct":
                            case "btDisDef":
                                oImport.SelectFile(pVal.ItemUID);
                                break;
                            case "btImport":
                                oImport.ProcessImportFiles();
                                break;
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void frmSalesOrderControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                SalesOrder oSales = new SalesOrder();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        oSales.AddFormItems(pVal.FormUID);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btDiscount":
                               
                                oSales.SetDiscountPercent(pVal.ItemUID);
                                
                                break;
                            case "1":
                                oSales.UpdateDiscountDetail(pVal.FormUID);
                                break;
                        }


                        break;
                    case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS:
                        switch (pVal.ItemUID)
                        {
                            case "38":
                                switch (pVal.ColUID)
                                {
                                    case "1":
                                    case "3":
                                    case "11":
                                    case "14":
                                    case "21":                                
                                        oSales.ResetFlag();
                                        break;
                                }
                                break;
                            case "4":
                            case "10":
                            case "40":
                                oSales.ResetFlag();
                                break;
                        }
                        break;

                }

            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }
        public void frmSalesOrderControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

     
            try
            {
                SalesOrder oSales = new SalesOrder();
                switch (pVal.EventType)
                {
  
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":

                               
                                oSales.SetDiscountPercent(pVal.ItemUID);
                                oSales.OpenOptionalDiscountAdding(pVal.FormUID);
                                BubbleEvent = oSales.ValidateDiscount();   
                                
                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:
                        switch (pVal.ItemUID)
                        {
                            case "38":
                                switch (pVal.ColUID)
                                {                                   
                                    case "15":
                                        BubbleEvent = false;
                                        break;
                                    default:
                                        BubbleEvent = true;
                                        break;
                                }
                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;
                    default:
                        BubbleEvent = true;
                        break;
                }
            
            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
      
        }

        public void frmB1SDISDETControlllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                SalesOrder oSalesOrder = new SalesOrder();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btOptDisc":
                                oSalesOrder.OpenOptionalDiscount();
                                break;
                        }
                        break;

                }

            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void frmInvoiceControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                Invoice oInvoice = new Invoice();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        oInvoice.AddFormItems(pVal.FormUID);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btDiscount":
                                oInvoice.GetInvoiceData(pVal.ItemUID);
                                break;
                        }
                        break;              
                }

            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void frmB1SOPTDISControlllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                SalesOrderOptDiscount oSalesOrderOpt = new SalesOrderOptDiscount();
                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                                oSalesOrderOpt.SaveOptionalDiscount();
                                break;
                        }
                        break;

                }

            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void frmB1SOSLDControllerAfter(string formUID, SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                SalesDiscount oSalesDiscount = new SalesDiscount();

                string stCode, stName;
                SAPbouiCOM.DataTable oDataTable;

                switch (pVal.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "btFind":
                                oSalesDiscount.LoadData();
                                break;
                            case "btDel":
                                oSalesDiscount.DeleteRow();
                                break;
                            case "41":
                                oSalesDiscount.SetApprovalUserReject();
                                break;
                            case "1000003":
                                oSalesDiscount.SetApprovalUserF();
                                break;
                            case "1000002":
                                oSalesDiscount.SetApprovalUser();
                                break;
                            case "0_U_G":
                                switch (pVal.ColUID)
                                {
                                    case "C_0_8":
                                        oSalesDiscount.OpenDiscountDetail(pVal.Row, "ADESD2");
                                    break;
                                }
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:

                        SAPbouiCOM.IChooseFromListEvent oCFLEvent;
                        oCFLEvent = (SAPbouiCOM.IChooseFromListEvent)pVal;

                        oDataTable = oCFLEvent.SelectedObjects;
                        switch (oCFLEvent.ChooseFromListUID)
                        {
                            case "CFL_6":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0).ToString();
                                    stName = oDataTable.GetValue(1, 0).ToString();

                                    oSalesDiscount.SetData("SN", pVal.Row, "", stCode, stName);
                                    
                                }
                                catch (Exception)
                                {

                                }

                                break;
                            case "CFL_5":
                                try
                                {
                                    stCode = oDataTable.GetValue(0, 0).ToString();
                                    stName = oDataTable.GetValue(1, 0).ToString();

                                    oSalesDiscount.SetData("INV", pVal.Row, "", stCode, stName);

                                }
                                catch (Exception)
                                {

                                }

                                break;

                        }
                        break;


                    case BoEventTypes.et_CLICK:
                        switch (pVal.ItemUID)
                        {
                            case "1_U_E":
                                oSalesDiscount.EnableFindField();
                                break;
                        }
                        break;


                }

            }

            catch (Exception ex)
            {
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }
        }
        public void frmB1SOSLDControllerBefore(string formUID, SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {


            try
            {
                SalesDiscount oSales = new SalesDiscount();
                switch (pVal.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        switch (pVal.ItemUID)
                        {
                            case "1":
                                BubbleEvent = oSales.CheckData();

                                break;
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoEventTypes.et_KEY_DOWN:
                        switch (pVal.ItemUID)
                        {
                            case "33_U_E":
           
                                        switch (pVal.CharPressed)
                                        {
                                            case 9:
                                                BubbleEvent = false;
                                                oSales.OpenCFLDiscounts("B1CFL", pVal.FormTypeEx, "DiscCode3", pVal.Row.ToString());
                                                break;
                                            default:
                                                BubbleEvent = true;
                                                break;
                                        }
                                        break;
                             
                            default:
                                BubbleEvent = true;
                                break;
                        }
                        break;
                    default:
                        BubbleEvent = true;
                        break;
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = true;
                Global.SetMessage("clsCatchingEvents. Form Advanced Discounts. " + ex.Message, Global.MsgType.Error);
            }

        }

    }
}
