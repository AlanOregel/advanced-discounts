﻿using System;
using SAPbobsCOM;
using SAPbouiCOM;
using Application = SAPbouiCOM.Application;
using Company = SAPbobsCOM.Company;

namespace AdvancedDiscounts
{
    public static class Global
    {
        public static Company oCompany;
        public static Application SBOApp;
        public static String csDirectory;
        public static bool flag = false;
        public enum MsgType
        {
            Error,
            Warning,
            Success
        }

        public static void SetMessage(String message, MsgType msgType)
        {
            switch (msgType)
            {
                case Global.MsgType.Error:
                    Global.SBOApp.StatusBar.SetSystemMessage(message, BoMessageTime.bmt_Short);
                    break;

                case Global.MsgType.Success:
                    Global.SBOApp.StatusBar.SetSystemMessage(message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    break;

                case Global.MsgType.Warning:
                    Global.SBOApp.StatusBar.SetSystemMessage(message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    break;

            }
        }

        public static Recordset Recordset()
        {
            try
            {
                Recordset oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                return oRecordset;
            }
            catch (Exception)
            {
                SetMessage("SetRecordSet", MsgType.Error);
                return null;
            }

        }

        public static void CreateForm(String csFormUID)
        {
            try
            {
                csFormUID = csFormUID + ".srf";
                LoadFromXML(ref csFormUID);
            }
            catch (Exception)
            {

            }
        }



        public static void LoadFromXML(ref string FileName)
        {

            System.Xml.XmlDocument oXmlDoc = null;

            oXmlDoc = new System.Xml.XmlDocument();

            string sPath = null;
            sPath = csDirectory;
            sPath = System.IO.Directory.GetParent(sPath).ToString();

            oXmlDoc.Load(sPath + "\\" + FileName);

            string sXML = oXmlDoc.InnerXml.ToString();
            Global.SBOApp.LoadBatchActions(ref sXML);

        }

        public static void AddMenu(String mainMenuId, BoMenuType menuType, string menuId, int position, string caption,
             string imgName)
        {
            SAPbouiCOM.Menus oMenus;
            SAPbouiCOM.MenuItem oMenuItem;

            oMenus = Global.SBOApp.Menus;

            if (oMenus.Exists(menuId))
            {
                return;
            }

            MenuCreationParams oCreationParams;
            oCreationParams = Global.SBOApp.CreateObject(BoCreatableObjectType.cot_MenuCreationParams);

            try
            {
                oMenuItem = Global.SBOApp.Menus.Item(mainMenuId);
                oMenus = oMenuItem.SubMenus;

                switch (menuType)
                {
                    case BoMenuType.mt_SEPERATOR:

                        oCreationParams.Type = BoMenuType.mt_SEPERATOR;
                        oCreationParams.UniqueID = menuId;
                        oCreationParams.Position = oMenuItem.SubMenus.Count + 1;
                        oCreationParams.Type = BoMenuType.mt_POPUP;
                        oCreationParams.String = caption;

                        if (imgName != "")
                        {
                            oCreationParams.Image = Global.csDirectory + imgName;
                        }

                        oMenus.AddEx(oCreationParams);

                        break;

                    case BoMenuType.mt_POPUP:
                        oCreationParams.Type = BoMenuType.mt_POPUP;
                        oCreationParams.UniqueID = menuId;
                        oCreationParams.String = caption;

                        if (position == -1)
                        {
                            oCreationParams.Position = oMenuItem.SubMenus.Count + 1;
                        }
                        else
                        {
                            oCreationParams.Position = position;
                        }

                        oMenus.AddEx(oCreationParams);

                        break;

                    case BoMenuType.mt_STRING:

                        oCreationParams.Type = BoMenuType.mt_STRING;
                        oCreationParams.UniqueID = menuId;
                        oCreationParams.String = caption;

                        if (position == -1)
                        {
                            oCreationParams.Position = oMenuItem.SubMenus.Count + 1;
                        }
                        else
                        {
                            oCreationParams.Position = position;
                        }


                        oMenus.AddEx(oCreationParams);
                        break;

                }

            }
            catch (Exception e)
            {
                Global.SetMessage("AddMenu. " + e.Message, Global.MsgType.Error);
            }
        }

        public static string GetProfile()
        {
            try
            {
                Recordset oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("EXEC B1S_AdvancedDiscounts 51, '" + oCompany.UserName + "'");
                oRecordset.MoveFirst();
                return oRecordset.Fields.Item(0).Value.ToString();

            }
            catch (Exception e)
            {
                SetMessage("GetProfile "+ e.Message, MsgType.Error);
                return "";
            }
        }

        public static string GetUserBP(String cardCode)
        {
            try
            {
                Recordset oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("EXEC B1S_AdvancedDiscounts 88, '" + oCompany.UserName + "','" + cardCode + "'");
                oRecordset.MoveFirst();
                return oRecordset.Fields.Item(0).Value.ToString();

            }
            catch (Exception e)
            {
                SetMessage("GetProfile " + e.Message, MsgType.Error);
                return "";
            }
        }

        public static string GetUserAUT2(String cardCode)
        {
            try
            {
                Recordset oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("EXEC B1S_AdvancedDiscounts 89, '" + oCompany.UserName + "','" + cardCode + "'");
                oRecordset.MoveFirst();
                return oRecordset.Fields.Item(0).Value.ToString();

            }
            catch (Exception e)
            {
                SetMessage("GetProfile " + e.Message, MsgType.Error);
                return "";
            }
        }

    }
}
