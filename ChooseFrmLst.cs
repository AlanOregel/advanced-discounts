﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class ChooseFrmLst
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;
        private string level;

        public void createForm(string form, string reform,string findtype, string row, string stParameter = "")
        {

            coForm = Global.SBOApp.Forms.Item(reform);

            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();

            oGrid = coForm.Items.Item("grData").Specific;
            coForm.DataSources.UserDataSources.Item("dsFrmRef").Value = reform;
            coForm.DataSources.UserDataSources.Item("dsRefType").Value = findtype;
            coForm.DataSources.UserDataSources.Item("dsRow").Value = row;

            coForm.DataSources.UserDataSources.Item("dsParam").Value = stParameter;

            switch (findtype)
            {
                case "Account":

                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 23" );
                    oGrid.Columns.Item("Codigo").Visible = false;
                    oGrid.Columns.Item("Cuenta").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Nombre").TitleObject.Sortable = true;
                    break;

                case "Account2":

                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 23");
                    oGrid.Columns.Item("Codigo").Visible = false;
                    oGrid.Columns.Item("Cuenta").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Nombre").TitleObject.Sortable = true;
                    break;

                case "TaxCode":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 45");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;

                    break;

                case "DiscCode":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 46");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;

                    break;

                case "DiscCode2":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 46");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;

                    break;

                case "DiscCode3":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 95, '" + stParameter + "'");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;

                    break;

                case "Address":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 24, '" + stParameter + "'");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Direccion").TitleObject.Sortable = true;
                    break;

                case "IncCode":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 25");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                    break;

                case "ExcCode":
                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 26");
                    oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                    oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                    break;
                case "LevelList":

                    level = stParameter;

                    switch (stParameter)
                    {
                        case "0":
                            oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 27");
                            break;
                        case "1":
                            oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 28");
                            break;
                        case "2":
                            oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 29");
                            break;
                        case "3":
                            oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 30");
                            break;
                        case "4":
                            oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 31");
                            break;
                        case "5":
                            oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 32");
                            break;
                    }

                    break;

            }




            oGrid.AutoResizeColumns();

            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.EditText loText;
                SAPbouiCOM.UserDataSource loDS;
                SAPbouiCOM.DataTable oDataTable;


                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    loDS = coForm.DataSources.UserDataSources.Add("dsFind", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtFind").Specific;
                    loText.DataBind.SetBound(true, "", "dsFind");

                    loDS = coForm.DataSources.UserDataSources.Add("dsFrmRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtFrmRef").Specific;
                    loText.DataBind.SetBound(true, "", "dsFrmRef");


                    loDS = coForm.DataSources.UserDataSources.Add("dsRefType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtValRef").Specific;
                    loText.DataBind.SetBound(true, "", "dsRefType");

                    loDS = coForm.DataSources.UserDataSources.Add("dsParam", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtParam").Specific;
                    loText.DataBind.SetBound(true, "", "dsParam");

                    loDS = coForm.DataSources.UserDataSources.Add("dsRow", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtRow").Specific;
                    loText.DataBind.SetBound(true, "", "dsRow");


                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }

         public void SetData()
        {
            try
            {
                SAPbouiCOM.Form coForm, auxForm;
                SAPbouiCOM.Grid auxGrid;
                SAPbouiCOM.Matrix auxMatrix;
                SAPbouiCOM.EditText oEdit;
                int index, auxindex;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oGrid = coForm.Items.Item("grData").Specific;

                if (oGrid.Rows.SelectedRows.Count == 0)
                {
                    return;
                }
                
                index = oGrid.Rows.SelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder);
                auxindex = oGrid.GetDataTableRowIndex(index);

              
                auxForm = Global.SBOApp.Forms.Item(coForm.DataSources.UserDataSources.Item("dsFrmRef").Value);
                auxForm.Freeze(true);


                switch (coForm.DataSources.UserDataSources.Item("dsRefType").Value)
                {
                    case "Account":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Cuenta", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        break;
                    case "Account2":
                        auxMatrix = auxForm.Items.Item("0_U_G").Specific;
                        int j = Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value);
                        oEdit = auxMatrix.Columns.Item("V_0").Cells.Item(j).Specific;
                        oEdit.Value = oGrid.DataTable.GetValue("Codigo", auxindex);
                        break;
                    case "TaxCode":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Codigo Impuesto", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        break;
                    case "DiscCode":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Codigo Descuento", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        break;
                    case "DiscCode2":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Descuento", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        auxGrid.DataTable.SetValue("Descripcion", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Descripcion", auxindex));
                        break;
                    case "DiscCode3":

                        auxForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DiscCode", 0, oGrid.DataTable.GetValue("Codigo", auxindex));
                        auxForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DiscName", 0, oGrid.DataTable.GetValue("Descripcion", auxindex));
                        auxForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DisApply", 0, oGrid.DataTable.GetValue("Porcentaje", auxindex));
                        auxForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DiscType", 0, oGrid.DataTable.GetValue("Tipo", auxindex));
                        auxForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DiscId", 0, oGrid.DataTable.GetValue("Id", auxindex));


                        break;

                    case "Address":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Codigo Direccion", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        break;
                    case "IncCode":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Codigo asignacion", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        break;
                    case "ExcCode":
                        auxGrid = auxForm.Items.Item("grData").Specific;
                        auxGrid.DataTable.SetValue("Codigo exclusion", Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value), oGrid.DataTable.GetValue("Codigo", auxindex));
                        break;
                    case "LevelList":
                        auxMatrix = auxForm.Items.Item("0_U_G").Specific;
                       
                        int i = Int32.Parse(coForm.DataSources.UserDataSources.Item("dsRow").Value);
                        oEdit = auxMatrix.Columns.Item("C_0_3").Cells.Item(i).Specific;
                        oEdit.Value = oGrid.DataTable.GetValue("Codigo", auxindex);

                        oEdit = auxMatrix.Columns.Item("C_0_4").Cells.Item(i).Specific;
                        oEdit.Value = oGrid.DataTable.GetValue("Descripcion", auxindex);

                        break;


                }


                coForm.Close();
                auxForm.Freeze(false);
            }
            catch (Exception e)
            {
      
                Global.SetMessage("SetData. " + e.Message, Global.MsgType.Error);
            }
        }

        public void FilterData()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                switch (coForm.DataSources.UserDataSources.Item("dsRefType").Value)
                {
                    case "Account":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 23, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").Visible = false;
                        oGrid.Columns.Item("Cuenta").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Nombre").TitleObject.Sortable = true;
                        break;
                    case "Account2":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 23, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").Visible = false;
                        oGrid.Columns.Item("Cuenta").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Nombre").TitleObject.Sortable = true;
                        break;
                    case "TaxCode":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 45, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                        break;

                    case "DiscCode":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 46, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                        break;
                    case "DiscCode2":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 46, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                        break;
                    case "Address":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 24, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Direccion").TitleObject.Sortable = true;
                        break;

                    case "IncCode":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 25, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                        break;

                    case "ExcCode":
                        oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 26, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                        oGrid.Columns.Item("Codigo").TitleObject.Sortable = true;
                        oGrid.Columns.Item("Descripcion").TitleObject.Sortable = true;
                        break;
                    case "LevelList":
                     
                        switch (coForm.DataSources.UserDataSources.Item("dsParam").Value)
                        {
                            case "0":
                                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 27, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                                break;
                            case "1":
                                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 28, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                                break;
                            case "2":
                                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 29, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                                break;
                            case "3":
                                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 30, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                                break;
                            case "4":
                                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 31, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                                break;
                            case "5":
                                oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 32, '" + coForm.DataSources.UserDataSources.Item("dsFind").Value + "'");
                                break;
                        }

                        break;
                }
            }
            catch (Exception e)
            {

                Global.SetMessage("FilterData. " + e.Message, Global.MsgType.Error);
            }
        }



    }
}
