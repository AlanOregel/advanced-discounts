﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class Monitor
    {

        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void createForm(string form)
        {
            Global.CreateForm(form);


            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();
          
            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.EditText loText;
                SAPbouiCOM.UserDataSource loDS;
                SAPbouiCOM.DataTable oDataTable;


                if (coForm.DataSources.UserDataSources.Count == 0)
                {


                    loDS = coForm.DataSources.UserDataSources.Add("dsFrom", SAPbouiCOM.BoDataType.dt_DATE);
                    loText = coForm.Items.Item("edtFrom").Specific;
                    loText.DataBind.SetBound(true, "", "dsFrom");

                    loDS = coForm.DataSources.UserDataSources.Add("dsSN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtCust").Specific;
                    loText.DataBind.SetBound(true, "", "dsSN");

                    loDS = coForm.DataSources.UserDataSources.Add("dsSN2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtCust2").Specific;
                    loText.DataBind.SetBound(true, "", "dsSN2");

                    loDS = coForm.DataSources.UserDataSources.Add("dsTo", SAPbouiCOM.BoDataType.dt_DATE);
                    loText = coForm.Items.Item("edtTo").Specific;
                    loText.DataBind.SetBound(true, "", "dsTo");

                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    coForm.DataSources.DataTables.Add("ProcData");

                    LoadComboStatus();
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }
        }

        public void LoadComboStatus()
        {
            try
            {

                SAPbouiCOM.ComboBox oCombo;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                oCombo = coForm.Items.Item("cmbStatus").Specific;

                if (oCombo.ValidValues.Count > 0)
                {
                    do
                    {
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                    }
                    while (oCombo.ValidValues.Count > 0);
                }

                oCombo.ValidValues.Add("TP2", "Inmediato Nota Crédito");
                oCombo.ValidValues.Add("TP3", "Facturas Acumuladas");
                oCombo.ValidValues.Add("TP4", "Opcional");
                oCombo.ValidValues.Add("TP5", "Volumen de  Venta");

                oCombo.Item.DisplayDesc = true;

                oCombo = coForm.Items.Item("cmbType").Specific;

                if (oCombo.ValidValues.Count > 0)
                {
                    do
                    {
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                    }
                    while (oCombo.ValidValues.Count > 0);
                }

                oCombo.ValidValues.Add("A", "Por Aprobar");
                oCombo.ValidValues.Add("P", "Por Procesar");
                oCombo.ValidValues.Add("S", "Procesados");
                oCombo.ValidValues.Add("E", "Error");
                oCombo.Item.DisplayDesc = true;
            }
            catch (Exception e)
            {
                Global.SetMessage("LoadComboTrigger. " + e.Message, Global.MsgType.Error);
            }
        }

        public void LoadData()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                EditText oStarDate, oEndDate;
                string stStrtDate, stEndDate;
                ComboBox oCombo, oComboType;
                SAPbouiCOM.GridColumn oColumn;
                SAPbouiCOM.ComboBoxColumn oCGC;
                SAPbouiCOM.EditTextColumn oColumns;

                oGrid = coForm.Items.Item("grData").Specific;
                oStarDate = coForm.Items.Item("edtFrom").Specific;
                oEndDate = coForm.Items.Item("edtTo").Specific;
                oCombo = coForm.Items.Item("cmbStatus").Specific;
                oComboType = coForm.Items.Item("cmbType").Specific;
                coForm.Freeze(true);

                stStrtDate = oStarDate.Value;
                stEndDate = oEndDate.Value;
                switch (oCombo.Selected.Value)
                {
                    case "TP3":
                        switch (oComboType.Selected.Value)
                        {

                            case "A":
                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 55, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("U_DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";
                                break;

                            case "P":
                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 54, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("U_DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";
                                break;
                            case "S":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 5, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                                oGrid.Columns.Item("DocEntry").Visible = false;
                                oGrid.AutoResizeColumns();

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";

                                SetColorEstatus(oGrid);

                                break;

                            case "E":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 65, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                                oGrid.Columns.Item("Id").Visible = false;
                                oGrid.AutoResizeColumns();

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";

                                break;

                        }
                        break;

                    case "TP2":
                        switch (oComboType.Selected.Value)
                        {
      
                            case "P":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 76, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Id");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";
                                break;

                            case "S":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 77, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DocEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "14";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";
                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                                SetColorEstatus(oGrid);
                                break;

                            case "E":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 78, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                                break;
                        }

                    break;


                    case "TP4":
                        switch (oComboType.Selected.Value)
                        {

                            case "P":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 79, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Id");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";
                                break;

                            case "S":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 80, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DocEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "14";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";
                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                                SetColorEstatus(oGrid);
                                break;

                            case "E":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 81, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                                break;
                        }

                        break;

                    case "TP5":

                        switch (oComboType.Selected.Value)
                        {

                            case "A":
                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 82, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("U_DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";
                                break;

                            case "P":
                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 83, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("U_DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";
                                break;

                            case "S":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 84, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                                oGrid.Columns.Item("DocEntry").Visible = false;
                                oGrid.AutoResizeColumns();

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";

                                SetColorEstatus(oGrid);

                                break;

                            case "E":

                                oGrid.DataTable.ExecuteQuery($"exec B1S_AdvancedDiscounts 85, '{coForm.DataSources.UserDataSources.Item("dsSN").Value}','{coForm.DataSources.UserDataSources.Item("dsSN2").Value}','{stStrtDate}','{stEndDate}'");
                                oColumn = oGrid.Columns.Item("Tipo Documento");
                                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");

                                oCGC.ValidValues.Add("PO", "Orden de Compra");
                                oCGC.ValidValues.Add("CM", "Nota de Credito");
                                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                                oGrid.Columns.Item("Id").Visible = false;
                                oGrid.AutoResizeColumns();

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("DestEntry");
                                oColumns.Width = 18;
                                oColumns.LinkedObjectType = "13";

                                oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cliente");
                                oColumns.LinkedObjectType = "2";

                                break;
                        }

                        break;

                }


                coForm.Freeze(false);
            }
            catch (Exception e)
            {

                coForm.Freeze(false);
                Global.SetMessage("LoadData. " + e.Message, Global.MsgType.Error);
            }


        }

     

        public void SetDatos(string stValue, string stChose, string stValue2 = "")
        {
            try
            {
                coForm = Global.SBOApp.Forms.Item("B1SADMON");

                switch (stChose)
                {
                    case "SN":
                        coForm.DataSources.UserDataSources.Item("dsSN").Value = stValue;
                        break;
                    case "SN2":
                        coForm.DataSources.UserDataSources.Item("dsSN2").Value = stValue;

                        break;
                    case "WHS":
                        coForm.DataSources.UserDataSources.Item("dsWHS").Value = stValue;
                        break;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetDatos. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SetColorEstatus(Grid oGrid)
        {
            int auxIndex;
            string estatus;
            try
            {
                for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                {
                    auxIndex = oGrid.GetDataTableRowIndex(i);
                    estatus = oGrid.DataTable.GetValue("Estado", auxIndex).ToString();
                    switch (estatus)
                    {
          
                        case "Generado":
                            oGrid.CommonSetting.SetCellFontColor((i + 1), 9, 888888);
                            break;
                        case "Reconciliado":
                            oGrid.CommonSetting.SetCellFontColor((i + 1), 9, 16711680);
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage(("Error asignar Color Estatus: " + ex.Message), Global.MsgType.Error);
            }

        }
    }
}
