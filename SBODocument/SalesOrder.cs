﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class SalesOrder
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void AddFormItems(String FormUID)
        {
            try
            {
                SAPbouiCOM.Item loItem;
                SAPbouiCOM.Button loButton;
                SAPbouiCOM.EditText loEdit;
                String lsItemRef;
                SAPbouiCOM.UserDataSource loDS;

                coForm = Global.SBOApp.Forms.Item(FormUID);
                lsItemRef = "2";

                loItem = coForm.Items.Add("btDiscount", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                loItem.Left = coForm.Items.Item(lsItemRef).Left + coForm.Items.Item(lsItemRef).Width + 5;
                loItem.Top = coForm.Items.Item(lsItemRef).Top;
                loItem.Width = coForm.Items.Item(lsItemRef).Width;
                loItem.Height = coForm.Items.Item(lsItemRef).Height;
                loButton = loItem.Specific;
                loButton.Caption = "Descuentos";

                loItem = coForm.Items.Add("edtFlag", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                loItem.Left = coForm.Items.Item(lsItemRef).Left + coForm.Items.Item(lsItemRef).Width + 100;
                loItem.Top = coForm.Items.Item(lsItemRef).Top;
                loItem.Width = 0;
                loItem.Height = coForm.Items.Item(lsItemRef).Height;
                loEdit = loItem.Specific;

                loDS = coForm.DataSources.UserDataSources.Add("dsFlag", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);               
                loEdit.DataBind.SetBound(true, "", "dsFlag");
                loEdit.Item.Visible = false;

                loItem = coForm.Items.Add("edtExecId", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                loItem.Left = coForm.Items.Item(lsItemRef).Left + coForm.Items.Item(lsItemRef).Width;
                loItem.Top = coForm.Items.Item(lsItemRef).Top - 10;
                loItem.Width = 0;
                loItem.Height = coForm.Items.Item(lsItemRef).Height;
                loEdit = loItem.Specific;

                loDS = coForm.DataSources.UserDataSources.Add("dsExecId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                loEdit.DataBind.SetBound(true, "", "dsExecId");
                loItem.Visible = false;

                loItem = coForm.Items.Add("edtExecId2", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                loItem.Left = coForm.Items.Item(lsItemRef).Left + coForm.Items.Item(lsItemRef).Width;
                loItem.Top = coForm.Items.Item(lsItemRef).Top - 10;
                loItem.Width = 0;
                loItem.Height = coForm.Items.Item(lsItemRef).Height;
                loEdit = loItem.Specific;

                loEdit.DataBind.SetBound(true, coForm.DataSources.DBDataSources.Item(0).TableName, "U_ExecId");
                loItem.Visible = false;

            }
            catch (Exception e)
            {
                Global.SetMessage("AddFormItems. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SetDiscountPercent(String ItemUID)
        {
            try
            {
                SAPbouiCOM.EditText oCardCode, oDocDate, oItemCode, oVisOrder, oDiscount, oExecId2 ;
                SAPbouiCOM.ComboBox oShiptoCode, oEditStatus;
                SAPbouiCOM.Matrix oMatrix;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
        

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                SAPbouiCOM.Item oItem = null;                
                oItem = coForm.Items.Item("81");
                oEditStatus = (SAPbouiCOM.ComboBox)oItem.Specific;

       
                if (coForm.TypeEx != "139")
                {
                    
                    return;
                }



                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE && oEditStatus.Selected.Value != "6")
                {
                                             
                    coForm.Freeze(true);
                    if (coForm.DataSources.UserDataSources.Item("dsFlag").Value != "Y")
                    {

                        oCardCode = coForm.Items.Item("4").Specific;
                        oMatrix = coForm.Items.Item("38").Specific;
                        oDocDate = coForm.Items.Item("10").Specific;
                        oShiptoCode = coForm.Items.Item("40").Specific;
                        oExecId2 = coForm.Items.Item("edtExecId2").Specific;

                        String ExecId = DateTime.Now.ToString("yyyyMMdhhmmss");


                        for (int i = 1; i < oMatrix.RowCount; i++)
                        {
                            oItemCode = oMatrix.Columns.Item("1").Cells.Item(i).Specific;
                            oVisOrder = oMatrix.Columns.Item("0").Cells.Item(i).Specific;
                            oDiscount = oMatrix.Columns.Item("15").Cells.Item(i).Specific;

                            oRecSet.DoQuery($"B1S_AdvancedDiscounts 1,'{oCardCode.Value.ToString()}','{oVisOrder.Value.ToString()}','{oItemCode.Value.ToString()}','{oDocDate.Value}','{oShiptoCode.Value.ToString()}','{ExecId}'");
                            oRecSet.MoveFirst();
                            oDiscount.Value = oRecSet.Fields.Item(0).Value.ToString();

                        }
                        coForm.DataSources.UserDataSources.Item("dsExecId").Value = ExecId;
                        oExecId2.Value = ExecId;
                        // coForm.DataSources.UserDataSources.Item("dsFlag").Value = "Y";
                        coForm.Freeze(false);
                        if (ItemUID == "btDiscount")
                        {
                            createForm("B1SDISDET", ExecId, "ADD", oCardCode.Value.ToString(), oDocDate.Value, oShiptoCode.Value);
                        }
                    }
                    coForm.Freeze(false);
                }
                else
                {
                    if (ItemUID == "btDiscount")
                    {
                        SAPbouiCOM.DBDataSource oDatatable;
                        string stTabla = "ORDR";
                        oExecId2 = coForm.Items.Item("edtExecId2").Specific;

                        oDatatable = coForm.DataSources.DBDataSources.Item(stTabla);
                        string stDocEntry = oDatatable.GetValue("U_ExecId", 0);
                        createForm("B1SDISDET", oExecId2.Value, "UPDATE", "", "", "");
                        coForm.Freeze(false);
                    }
                }

   

            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage("SetDiscountPercent. " + e.Message, Global.MsgType.Error);
            }
        }

        public void ResetFlag()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                if (coForm.TypeEx != "139")
                {
                    return;
                }
                coForm.DataSources.UserDataSources.Item("dsFlag").Value = "N";
            }
            catch (Exception e)
            {
                Global.SetMessage("ResetFlag. " + e.Message, Global.MsgType.Error);
            }
        }

        public void createForm(string form, string ExecId, string mode, String stCardCode, String stDocDate, String stShiptoCode)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources(ExecId, mode, stCardCode, stDocDate, stShiptoCode);
            coForm.Freeze(false);
        }

        public void BindUserDataSources(string ExecId, string mode, String stCardCode, String stDocDate, String stShiptoCode)
        {
            try
            {
                SAPbouiCOM.DataTable oDataTable;
                SAPbouiCOM.UserDataSource loDS;
                SAPbouiCOM.EditText loEdit;

                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    oGrid.DataTable.ExecuteQuery($"EXEC B1S_AdvancedDiscounts 3, '{ExecId}','{mode}'");

                    oGrid.CollapseLevel = 1;
                    oGrid.CommonSetting.FixedColumnsCount = 3;

                    for (int i = 0; i <= oGrid.Rows.Count - 1; i++)
                    {
                        if (!oGrid.Rows.IsLeaf(i))
                        {

                        }
                        else
                        {
                            oGrid.CommonSetting.SetCellFontColor(i + 1, 2, 16250871);
                        }
                    }

                    oGrid.AutoResizeColumns();
                    loEdit = coForm.Items.Item("edtExecId").Specific;
                    loDS = coForm.DataSources.UserDataSources.Add("dsExecId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loDS = coForm.DataSources.UserDataSources.Add("dsMode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loDS = coForm.DataSources.UserDataSources.Add("dsSN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loDS = coForm.DataSources.UserDataSources.Add("dsDocDate", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loDS = coForm.DataSources.UserDataSources.Add("dsShpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loEdit.DataBind.SetBound(true, "", "dsExecId");

                    loEdit.Value = ExecId;

                    coForm.DataSources.UserDataSources.Item("dsMode").Value = mode;
                    coForm.DataSources.UserDataSources.Item("dsSN").Value = stCardCode;
                    coForm.DataSources.UserDataSources.Item("dsDocDate").Value = stDocDate;
                    coForm.DataSources.UserDataSources.Item("dsShpCod").Value = stShiptoCode;
                }


            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }

        public bool ValidateDiscount()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                if (coForm.TypeEx != "139")
                {
                    return false;
                }

                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    SAPbouiCOM.Item oItem = null;
                    oItem = coForm.Items.Item("81");
                   SAPbouiCOM.ComboBox oEditStatus = (SAPbouiCOM.ComboBox)oItem.Specific;

                    if (oEditStatus.Selected.Value == "6")
                    {
                        return true;
                    }

                    if (coForm.DataSources.UserDataSources.Item("dsFlag").Value == "Y")
                    {
                        return true;
                    }
                    else
                    {
                        Global.SetMessage("No se ha asignado descuentos para el documento.", Global.MsgType.Warning);
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("ValidateDiscount. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }


        public void UpdateDiscountDetail(string FormUID)
        {
            try
            {
                
                coForm = Global.SBOApp.Forms.Item(FormUID);

                SAPbouiCOM.Item oItem = null;
                oItem = coForm.Items.Item("81");
                SAPbouiCOM.ComboBox oEditStatus = (SAPbouiCOM.ComboBox)oItem.Specific;

                //if (oEditStatus.Selected.Value != "6" && coForm.DataSources.UserDataSources.Item("dsFlag").Value == "Y" && coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                //{
                   
       
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbouiCOM.DBDataSource oDatatable;
                string stTabla = "ORDR";
                oDatatable = coForm.DataSources.DBDataSources.Item(stTabla);
                string stDocEntry = oDatatable.GetValue("DocEntry", 0);

                oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 4, '{stDocEntry}','{coForm.DataSources.UserDataSources.Item("dsExecId").Value}'");
               // }

            }
            catch (Exception e)
            {
                Global.SetMessage("UpdateDiscountDetail. " + e.Message, Global.MsgType.Error);
            }
        }

        public void OpenOptionalDiscount()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                SalesOrderOptDiscount oOptDisc = new SalesOrderOptDiscount();
                oOptDisc.createForm("B1SOPTDIS", 
                                     coForm.DataSources.UserDataSources.Item("dsExecId").Value, 
                                     coForm.DataSources.UserDataSources.Item("dsMode").Value, 
                                     coForm.DataSources.UserDataSources.Item("dsSN").Value, 
                                     coForm.DataSources.UserDataSources.Item("dsDocDate").Value, 
                                     coForm.DataSources.UserDataSources.Item("dsShpCod").Value,"");

            }
            catch (Exception e)
            {
                Global.SetMessage("OpenOptionalDiscount. " + e.Message, Global.MsgType.Error);
            }
        }

        public void OpenOptionalDiscountAdding(String frmRef)
        {
            SAPbouiCOM.EditText oCardCode, oDocDate;
            SAPbouiCOM.ComboBox oShiptoCode, oEditStatus;
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            try
            {
                oCardCode = coForm.Items.Item("4").Specific;
                oDocDate = coForm.Items.Item("10").Specific;
                oShiptoCode = coForm.Items.Item("40").Specific;


                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    SAPbouiCOM.Item oItem = null;
                    oItem = coForm.Items.Item("81");
                    oEditStatus = (SAPbouiCOM.ComboBox)oItem.Specific;

                    if (oEditStatus.Selected.Value == "6")
                    {
                        return;
                    }

                    if (coForm.DataSources.UserDataSources.Item("dsFlag").Value == "Y")
                    {
                        coForm.Freeze(false);
                        return;
                    }


                    oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 6, '{oCardCode.Value}','', '{oDocDate.Value}', '{oShiptoCode.Selected.Value}', '{coForm.DataSources.UserDataSources.Item("dsExecId").Value}','ADD'");
                    if (oRecSet.RecordCount > 0)
                    {
                        coForm.DataSources.UserDataSources.Item("dsFlag").Value = "N";

                        coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                        SalesOrderOptDiscount oOptDisc = new SalesOrderOptDiscount();
                        oOptDisc.createForm("B1SOPTDIS",
                                             coForm.DataSources.UserDataSources.Item("dsExecId").Value,
                                             "ADD",
                                             oCardCode.Value.ToString(),
                                             oDocDate.Value,
                                             oShiptoCode.Selected.Value, frmRef);
                    }
                    else
                    {
                        coForm.DataSources.UserDataSources.Item("dsFlag").Value = "Y";
                    }

                }
            }
            catch (Exception e)
            {
                Global.SetMessage("OpenOptionalDiscount. " + e.Message, Global.MsgType.Error);
            }
        }
    }
}
