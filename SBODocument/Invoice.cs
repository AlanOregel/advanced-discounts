﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class Invoice
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void AddFormItems(String FormUID)
        {
            try
            {
                SAPbouiCOM.Item loItem;
                SAPbouiCOM.Button loButton;
                String lsItemRef;

                coForm = Global.SBOApp.Forms.Item(FormUID);
                lsItemRef = "2";

                loItem = coForm.Items.Add("btDiscount", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                loItem.Left = coForm.Items.Item(lsItemRef).Left + coForm.Items.Item(lsItemRef).Width + 5;
                loItem.Top = coForm.Items.Item(lsItemRef).Top;
                loItem.Width = coForm.Items.Item(lsItemRef).Width;
                loItem.Height = coForm.Items.Item(lsItemRef).Height;
                loButton = loItem.Specific;
                loButton.Caption = "Descuentos";

            }
            catch (Exception e)
            {
                Global.SetMessage("AddFormItems. " + e.Message, Global.MsgType.Error);
            }
        }


        public void GetInvoiceData(String ItemUID)
        {
            try
            {
                SAPbouiCOM.EditText oCardCode, oDocDate;
                SAPbouiCOM.ComboBox oShiptoCode;
                SAPbouiCOM.Matrix oMatrix;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                if (coForm.TypeEx != "133")
                {
                    return;
                }

                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {


                    coForm.Freeze(true);
                    oCardCode = coForm.Items.Item("4").Specific;
                    oMatrix = coForm.Items.Item("38").Specific;
                    oDocDate = coForm.Items.Item("10").Specific;
                    oShiptoCode = coForm.Items.Item("40").Specific;

                    String ExecId = DateTime.Now.ToString("yyyyMMdhhmmss");

                    coForm.Freeze(false);

                    if (ItemUID == "btDiscount")
                    {
                        createForm("B1SDISDET", ExecId, "ADD", oCardCode.Value.ToString(), oDocDate.Value, oShiptoCode.Value);
                    }
                }
                else
                {
                    if (ItemUID == "btDiscount")
                    {
                        SAPbouiCOM.DBDataSource oDatatable;
                        string stTabla = "OINV";
                        oDatatable = coForm.DataSources.DBDataSources.Item(stTabla);
                        string stDocEntry = oDatatable.GetValue("DocEntry", 0);
                        createForm("B1SDISDET", stDocEntry, "UPDATE", "", "", "");
                    }
                }



            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage("SetDiscountPercent. " + e.Message, Global.MsgType.Error);
            }
        }


        public void createForm(string form, string ExecId, string mode, String stCardCode, String stDocDate, String stShiptoCode)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources(ExecId, mode, stCardCode, stDocDate, stShiptoCode);
            coForm.Freeze(false);
        }

        public void BindUserDataSources(string ExecId, string mode, String stCardCode, String stDocDate, String stShiptoCode)
        {
            try
            {
                SAPbouiCOM.DataTable oDataTable;
                SAPbouiCOM.Button loButton;
                SAPbouiCOM.GridColumn oColumn;
                SAPbouiCOM.ComboBoxColumn oCGC;

                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    oGrid.DataTable.ExecuteQuery($"EXEC B1S_AdvancedDiscounts 8, '{stCardCode}','', '{stDocDate}', '{stShiptoCode}', '{ExecId}','{mode}'");

                    oColumn = oGrid.Columns.Item("Tipo Valor");
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                    oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Valor");
                    oCGC.ValidValues.Add("I", "Importe");
                    oCGC.ValidValues.Add("P", "Porcentaje");
                    oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                    oGrid.AutoResizeColumns();

                    loButton = coForm.Items.Item("btOptDisc").Specific;
                    loButton.Item.Visible = false;
                }


            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }

        public void CreateCMDetail(string FormUID)
        {

                try
                {
                    coForm = Global.SBOApp.Forms.Item(FormUID);
                    SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                    SAPbouiCOM.DBDataSource oDatatable;
                    string stTabla = "OINV";
                    oDatatable = coForm.DataSources.DBDataSources.Item(stTabla);
                    string stDocEntry = oDatatable.GetValue("DocEntry", 0);

                    oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 9, '{stDocEntry}','{DateTime.Now.ToString("yyyyMMdhhmmss")}'");
                    oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 10, '{stDocEntry}','{DateTime.Now.ToString("yyyyMMdhhmmss")}'");
            }
                catch (Exception e)
                {
                    Global.SetMessage("UpdateDiscountDetail. " + e.Message, Global.MsgType.Error);
                }
        }
    }
}
