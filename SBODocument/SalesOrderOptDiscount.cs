﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class SalesOrderOptDiscount
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;
        public void createForm(string form, string ExecId, string mode, String stCardCode, String stDocDate, String stShiptoCode, String frmRef)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources(ExecId, mode, stCardCode, stDocDate, stShiptoCode, frmRef);
            coForm.Freeze(false);
        }


        public void BindUserDataSources(string ExecId, string mode, String stCardCode, String stDocDate, String stShiptoCode, String frmRef)
        {
            try
            {
                SAPbouiCOM.DataTable oDataTable;
                SAPbouiCOM.GridColumn oColumn;
                SAPbouiCOM.ComboBoxColumn oCGC;
                SAPbouiCOM.UserDataSource loDS;
                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    loDS = coForm.DataSources.UserDataSources.Add("dsRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    coForm.DataSources.UserDataSources.Item("dsRef").Value = frmRef;

                    if (mode == "ADD")
                    {
                        oGrid.DataTable.ExecuteQuery($"EXEC B1S_AdvancedDiscounts 6, '{stCardCode}','', '{stDocDate}', '{stShiptoCode}', '{ExecId}','{mode}'");
                    }
                    else
                    {
                        oGrid.DataTable.ExecuteQuery($"EXEC B1S_AdvancedDiscounts 6, '{stCardCode}','', '{stDocDate}', '{stShiptoCode}', '{ExecId}', '{mode}'");
                    }

                    oGrid.Columns.Item("CardCode").Visible = false;
                    oGrid.Columns.Item("DocEntry").Visible = false;
                    oGrid.Columns.Item("ExecId").Visible = false;
                    oGrid.Columns.Item("Generated").Visible = false;
                    oGrid.Columns.Item("CreateDate").Visible = false;
                    oGrid.Columns.Item("IncludeList").Visible = false;
                    oGrid.Columns.Item("ExcludeList").Visible = false;
                    oGrid.Columns.Item("Select").Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;

                    if (frmRef == "")
                    {
                        oGrid.Columns.Item("Select").Visible = false;
                    }

                    oColumn = oGrid.Columns.Item("Tipo Valor");
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                    oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Valor");
                    oCGC.ValidValues.Add("I", "Importe");
                    oCGC.ValidValues.Add("P", "Porcentaje");
                    oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                    oGrid.Columns.Item("Codigo Descuento").Editable = false;
                    oGrid.Columns.Item("Descripcion").Editable = false;
                    oGrid.Columns.Item("Tipo").Editable = false;
                    oGrid.Columns.Item("Tipo Valor").Editable = false;
                    oGrid.Columns.Item("Valor").Editable = false;

                    coForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;

                    if (mode != "ADD")
                    {
                        oGrid.Columns.Item("Select").Editable = false;
                        coForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                    }

                    oColumn = oGrid.Columns.Item("Calculo");
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                    oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Calculo");
                    oCGC.ValidValues.Add("S", "Subtotal");
                    oCGC.ValidValues.Add("C", "Cascada");
                    oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                    oColumn.Editable = false;
                    oGrid.AutoResizeColumns();
                }


            } 
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }

        public void OpenOptionalDiscount(String ItemUID)
        {
            try
            {
                SAPbouiCOM.EditText oCardCode, oDocDate;
                SAPbouiCOM.ComboBox oShiptoCode;

                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                String stCardCode, stDocDate, stShiptoCode;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                if (coForm.TypeEx != "139")
                {
                    return;
                }

                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    coForm.Freeze(true);

                    oCardCode = coForm.Items.Item("4").Specific;
                    stCardCode = oCardCode.Value;
     
                    oDocDate = coForm.Items.Item("10").Specific;
                    stDocDate = oDocDate.Value;

                    oShiptoCode = coForm.Items.Item("40").Specific;
                    stShiptoCode = oShiptoCode.Value;

                    String ExecId = coForm.DataSources.UserDataSources.Item("dsExecId").Value;             

                    coForm.Freeze(false);
                    if (ItemUID == "btDiscount")
                    {
                        createForm("B1SOPTDIS", ExecId, "ADD",stCardCode,stDocDate,stShiptoCode,"");
                    }
                }
            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage("SetDiscountPercent. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SaveOptionalDiscount()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                String stCardCode, stDocEntry, stDiscCode, stDiscName, stDiscType, stValType, stExecId, stIncludeList, stExcludeList, stCalcType;
                double stValue;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                SAPbouiCOM.Form auxForm;

                for (int i = 0; i< oGrid.Rows.Count; i++)
                {
                    if (oGrid.DataTable.GetValue("Select", i) == "Y")
                    {
                      
                        stCardCode = oGrid.DataTable.GetValue("CardCode", i);
                        stDocEntry = oGrid.DataTable.GetValue("DocEntry", i).ToString();
                        stDiscCode = oGrid.DataTable.GetValue("Codigo Descuento", i);
                        stDiscName = oGrid.DataTable.GetValue("Descripcion", i);
                        stDiscType = oGrid.DataTable.GetValue("Tipo", i);
                        stValType = oGrid.DataTable.GetValue("Tipo Valor", i);
                        stValue = oGrid.DataTable.GetValue("Valor", i);
                        stExecId = oGrid.DataTable.GetValue("ExecId", i);
                        stIncludeList = oGrid.DataTable.GetValue("IncludeList", i);
                        stExcludeList = oGrid.DataTable.GetValue("ExcludeList", i);
                        stCalcType = oGrid.DataTable.GetValue("Calculo", i);
                        oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 7, '{stCardCode}','{stDocEntry}','{stDiscCode}','{stDiscName}','{stDiscType}','{stValType}','{stValue.ToString()}','{stExecId}','{stIncludeList}','{stExcludeList}','{stCalcType}'");
                    }

                }

                if (coForm.DataSources.UserDataSources.Item("dsRef").Value != "")
                {
                    auxForm = Global.SBOApp.Forms.Item(coForm.DataSources.UserDataSources.Item("dsRef").Value);
                    auxForm.DataSources.UserDataSources.Item("dsFlag").Value = "Y";

                }

                coForm.Close();
            }
            catch (Exception e)
            {
                Global.SetMessage("SaveOptionalDiscount. " + e.Message, Global.MsgType.Error);
            }
        }
    }
}
