﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class SalesDiscount
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;
        public void createForm(string form)
        {
            EditText oEdit;
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

            AddChooseFromList();

            oEdit = coForm.Items.Item("26_U_E").Specific;
            oEdit.ChooseFromListUID = "CFL_5";
            oEdit.ChooseFromListAlias = "DocEntry";

            oEdit = coForm.Items.Item("20_U_E").Specific;
            oEdit.ChooseFromListUID = "CFL_6";
            oEdit.ChooseFromListAlias = "CardCode";


            coForm.Freeze(true);
            coForm.Freeze(false);
        }


        public void SetData(string stChose, int row, string column, string value, string description)
        {
            try


            {
                coForm = Global.SBOApp.Forms.Item("B1SOSLD");
                SAPbouiCOM.Matrix oMatrix = coForm.Items.Item("0_U_G").Specific;
                SAPbouiCOM.EditText oEditText;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                switch (stChose)
                {
                    case "SN":

                        coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_CardCode", 0, value);
                        coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_CardName", 0, description);
                        coForm.Items.Item("20_U_E").Enabled = true;
                        coForm.Items.Item("btFind").Enabled = true;
                        coForm.Items.Item("btDel").Enabled = true;
                        break;
                    case "INV":

                        coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DestEntry", 0, value);
                        coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DestNum", 0, description);

                        break;

                    case "ACT":
                        try
                        {
                            oEditText = oMatrix.Columns.Item(column).Cells.Item(row).Specific;
                            oEditText.Value = value;
                        }
                        catch (Exception)
                        {
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetData. " + e.Message, Global.MsgType.Error);
            }
        }

        public void LoadData()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                SAPbouiCOM.EditText oEdit;
                SAPbouiCOM.CheckBox oCheckBox;
                String CardCode, DiscDate, ExecId;
                double discTotal, salTotal;
                
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                coForm.Freeze(true);

                ExecId = DateTime.Now.ToString("yyyyMMdhhmmss");
                coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_ExecId", 0, ExecId);


                oEdit = coForm.Items.Item("20_U_E").Specific;
                CardCode = oEdit.Value;
                oEdit = coForm.Items.Item("24_U_E").Specific;
                DiscDate = oEdit.Value;

                oMatrix = coForm.Items.Item("0_U_G").Specific;
                oMatrix.Clear();

                oRecSet.DoQuery("EXEC B1S_ADTP5 0, '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_DiscId", 0) +
                                "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_DiscCode", 0) +
                                "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_DiscName", 0) +
                                "', '" + ExecId +
                                 "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_DisApply", 0) +
                                 "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_DiscType", 0) +
                                 "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_StartDate", 0) +
                                 "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_EndDate", 0) +
                                 "', '" + coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_CardCode", 0) + "'"
                                );
                oRecSet.MoveFirst();

                oMatrix.Columns.Item("C_0_4").Editable = true;
                salTotal = 0;
                discTotal = 0;

                for (int i = 1; i <= oRecSet.RecordCount; i++)
                {
                    oMatrix.AddRow();


                    oEdit = oMatrix.Columns.Item("C_0_3").Cells.Item(i).Specific;
                    oEdit.Value = i.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_2").Cells.Item(i).Specific;
                    oEdit.Value = i.ToString();


                    oEdit = oMatrix.Columns.Item("C_0_8").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("Marca").Value.ToString();

                    oEdit = oMatrix.Columns.Item("C_0_4").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("Total venta").Value.ToString();
                    salTotal = salTotal + oRecSet.Fields.Item("Total venta").Value;

                    oEdit = oMatrix.Columns.Item("C_0_5").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("Total Descuento").Value.ToString();
                    discTotal = discTotal + oRecSet.Fields.Item("Total Descuento").Value;

                    oEdit = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific;
                    oEdit.Value = oRecSet.Fields.Item("CodMarca").Value.ToString();


                    oRecSet.MoveNext();
                }

                coForm.Items.Item("40").Click();
                oMatrix.Columns.Item("C_0_4").Editable = false;

                oRecSet.DoQuery("B1S_AdvancedDiscounts 86, '" + CardCode + "','" + ExecId + "'");
                oRecSet.MoveFirst();

                coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DestNum", 0, oRecSet.Fields.Item("DocNum").Value);
                coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DestEntry", 0, oRecSet.Fields.Item("DocEntry").Value);
                coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_SalTotal", 0, salTotal.ToString());
                coForm.DataSources.DBDataSources.Item("@B1S_OSLD").SetValue("U_DisTotal", 0, discTotal.ToString());


                coForm.Freeze(false);
            }
            catch (Exception ex)
            {
                coForm.Freeze(false);
                Global.SetMessage("Special Discount. LoadData. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void OpenDiscountDetail(int row, string form)
        {
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            SAPbouiCOM.Matrix oMatrix = coForm.Items.Item("0_U_G").Specific;


            if(row > oMatrix.RowCount || row <= 0)
            {
                return;
            }

            SAPbouiCOM.EditText oEditText = oMatrix.Columns.Item("V_0").Cells.Item(row).Specific;
            string execid = coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_ExecId", 0);
            string brand = oEditText.Value.ToString();
            Global.CreateForm(form);
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            SAPbouiCOM.EditTextColumn oColumn;
            try
            {

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                coForm.Freeze(true);

                coForm.Width = 630;

                BindUserDataSources();
                oGrid = coForm.Items.Item("grData").Specific;
                oGrid.Item.Width = 600;
                oGrid.DataTable.ExecuteQuery("EXEC B1S_ADTP5 1, '" + brand + "', '" + execid + "'");

                oColumn = (SAPbouiCOM.EditTextColumn) oGrid.Columns.Item("Factura");
                oColumn.LinkedObjectType = "13";

                oGrid.AutoResizeColumns();  
                coForm.Freeze(false);

            }
            catch (Exception ex)
            {
                coForm.Freeze(false);
                Global.SetMessage("Special Discount. LoadData. " + ex.Message, Global.MsgType.Error);
            }
        }


        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.DataTable oDataTable;

                if (coForm.DataSources.UserDataSources.Count == 0)
                {

                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }
        }

        //public void EnableCheckButtons()
        //{
        //    try
        //    {
        //        SAPbouiCOM.CheckBox oCheckBox, oCheckBox2;
        //        coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

        //        oCheckBox = coForm.Items.Item("1000002").Specific;
        //        oCheckBox2 = coForm.Items.Item("25").Specific;

        //        if (Global.GetProfile() == "MAN" || Global.GetProfile() == "AUT")
        //        {
        //            if (oCheckBox.Checked || oCheckBox2.Checked)
        //            {
        //                coForm.Items.Item("1000002").Enabled = false;
        //                coForm.Items.Item("25").Enabled = false;
        //                coForm.Items.Item("btDel").Enabled = false;
        //                coForm.Items.Item("0_U_G").Enabled = false;

        //            }
        //            else
        //            {
        //                coForm.Items.Item("1000002").Enabled = true;
        //                coForm.Items.Item("25").Enabled = true;
        //                coForm.Items.Item("btDel").Enabled = true;
        //                coForm.Items.Item("0_U_G").Enabled = true;
        //            }
        //        }
        //        else
        //        {
        //            coForm.Items.Item("1000002").Enabled = false;
        //            coForm.Items.Item("25").Enabled = false;
        //            coForm.Items.Item("0_U_G").Enabled = true;

        //            if (oCheckBox.Checked || oCheckBox2.Checked)
        //            {
        //                coForm.Items.Item("btDel").Enabled = false;
        //                coForm.Items.Item("0_U_G").Enabled = false;
        //            }

        //        }

        //        coForm.Items.Item("24_U_E").Enabled = false;
        //        coForm.Items.Item("btFind").Enabled = false;

        //        //staticText = coForm.Items.Item("lblCancel").Specific;
        //        //if (coForm.DataSources.DBDataSources.Item("@B1S_OESD").GetValue("Canceled", 0).ToString() == "Y")
        //        //{
        //        //    staticText.Caption = "Cancelado";
        //        //}
        //        //else
        //        //{
        //        //    staticText.Caption = "";
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        Global.SetMessage("EnableCheckButtons. " + ex.Message, Global.MsgType.Error);
        //    }
        //}

        public void SendMessaage()
        {
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            SAPbouiCOM.DBDataSource oDatatable;
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            oDatatable = coForm.DataSources.DBDataSources.Item("@B1S_OSLD");
            string stDocEntry = oDatatable.GetValue("DocEntry", 0);

            SAPbobsCOM.CompanyService oComService;
            SAPbobsCOM.MessagesService oMsgService;
            SAPbobsCOM.Message oMessage;

            oComService = Global.oCompany.GetCompanyService();
            oMsgService = (SAPbobsCOM.MessagesService)oComService.GetBusinessService(SAPbobsCOM.ServiceTypes.MessagesService);
            oMessage = (SAPbobsCOM.Message)oMsgService.GetDataInterface(SAPbobsCOM.MessagesServiceDataInterfaces.msdiMessage);
            oMessage.User = 1;     // existing user with email set
            oMessage.Subject = "Nuevo descuento por volumen de venta pendiente de aprobar";
            oMessage.Text = "Documento: " + stDocEntry + "  Fecha: " + System.DateTime.Now.ToString();


            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 64");
            oRecSet.MoveFirst();
            SAPbobsCOM.RecipientCollection cEmailAddress = oMessage.RecipientCollection;

            for (int i = 0; i <= oRecSet.RecordCount - 1; i++)
            {
                cEmailAddress.Add();
                cEmailAddress.Item(i).SendInternal = SAPbobsCOM.BoYesNoEnum.tYES;
                cEmailAddress.Item(i).UserCode = oRecSet.Fields.Item("USER_CODE").Value.ToString();
                oRecSet.MoveNext();
            }

            SAPbobsCOM.MessageHeader oMsgHead = oMsgService.SendMessage(oMessage);

        }

        public void DeleteRow()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                SAPbobsCOM.Recordset oRecSet;
                String ExecId;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oMatrix = coForm.Items.Item("0_U_G").Specific;
                oRecSet = Global.Recordset();

               
                ExecId = coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_ExecId", 0);

                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    if (oMatrix.IsRowSelected(i))
                    {
                        
                        oMatrix.DeleteRow(i);

                        oRecSet.DoQuery("B1S_AD_SalesDisc 2, '" + ExecId + "','" + i.ToString() + "'");
                    }
                }

                oMatrix.FlushToDataSource();
                if (coForm.Mode != SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    coForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }
             

            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }



        public void AddChooseFromList()
        {
            try
            {
                ChooseFromListCollection oCFLs;
                Conditions oCons;
                Condition oCon;
                ChooseFromList oCFL;
                ChooseFromListCreationParams oCFLCreationParams;

                oCFLs = coForm.ChooseFromLists;
                oCFLCreationParams = Global.SBOApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams);


                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "13";
                oCFLCreationParams.UniqueID = "CFL_5";

                oCFL = oCFLs.Add(oCFLCreationParams);

                oCons = oCFL.GetConditions();

                oCon = oCons.Add();
                oCon.Alias = "DocStatus";
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCon.CondVal = "O";

                oCFL.SetConditions(oCons);


                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "2";
                oCFLCreationParams.UniqueID = "CFL_6";

                oCFL = oCFLs.Add(oCFLCreationParams);


                oCons = oCFL.GetConditions();

                oCon = oCons.Add();
                oCon.Alias = "CardType";
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCon.CondVal = "C";

                oCFL.SetConditions(oCons);


            }
            catch (Exception ex)
            {
                coForm.Freeze(false);
                Global.SetMessage("AddChooseFrmList. " + ex.Message, Global.MsgType.Error);
            }
        }




        //public void SetApprovalUser()
        //{
        //    try
        //    {

        //        SAPbouiCOM.CheckBox oCheckBox, oCheckBox2;
        //        SAPbouiCOM.EditText oEdit;
        //        coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

        //        oCheckBox = coForm.Items.Item("1000002").Specific;
        //        oCheckBox2 = coForm.Items.Item("25").Specific;
        //        oEdit = coForm.Items.Item("25_U_E").Specific;

        //        if (oCheckBox.Checked || oCheckBox2.Checked)
        //        {
        //            oEdit.Value = Global.oCompany.UserName;
        //            coForm.Items.Item("btDel").Enabled = false;
        //        }

        //        if (!oCheckBox.Checked && !oCheckBox2.Checked)
        //        {
        //            oEdit.Value = "";
        //            coForm.Items.Item("btDel").Enabled = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
        //    }
        //}

        public bool CheckData()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    oMatrix = coForm.Items.Item("0_U_G").Specific;
                    if (oMatrix.RowCount > 0)
                    {
                        return true;
                    }   
                    else
                    {
                        Global.SetMessage("No hay registros. ", Global.MsgType.Warning);
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("CheckData. " + ex.Message, Global.MsgType.Error);
                return false;
            }
        }


        public void EnableFindField()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                if (coForm.Mode == SAPbouiCOM.BoFormMode.fm_FIND_MODE)
                {
                    coForm.Items.Item("1_U_E").Enabled = true;
                }
                else
                {
                    coForm.Items.Item("1_U_E").Enabled = false;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetDatos. " + e.Message, Global.MsgType.Error);
            }
        }


        public void SetApprovalUser()
        {
            try
            {

                SAPbouiCOM.CheckBox oCheckBox;
                SAPbouiCOM.EditText oEdit;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBox = coForm.Items.Item("1000002").Specific;
                oEdit = coForm.Items.Item("24_U_E").Specific;

                if (oCheckBox.Checked)
                {
                    oEdit.Value = Global.oCompany.UserName;
                    coForm.Items.Item("btDel").Enabled = false;
                    coForm.Items.Item("btFind").Enabled = false;
                }


                if (!oCheckBox.Checked)
                {
                    oEdit.Value = "";
                    coForm.Items.Item("btDel").Enabled = true;
                    coForm.Items.Item("btFind").Enabled = false;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void SetApprovalUserF()
        {
            try
            {

                SAPbouiCOM.CheckBox oCheckBox2;
                SAPbouiCOM.EditText oEditF;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBox2 = coForm.Items.Item("1000003").Specific;
                oEditF = coForm.Items.Item("30_U_E").Specific;

                if (oCheckBox2.Checked)
                {
                    oEditF.Value = Global.oCompany.UserName;
                    coForm.Items.Item("btDel").Enabled = false;
                    coForm.Items.Item("btFind").Enabled = false;
                }


                if (!oCheckBox2.Checked)
                {
                    oEditF.Value = "";
                    coForm.Items.Item("btDel").Enabled = true;
                    coForm.Items.Item("btFind").Enabled = false;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void SetApprovalUserReject()
        {
            try
            {

                SAPbouiCOM.CheckBox oCheckBoxF;
                SAPbouiCOM.EditText oEditF;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBoxF = coForm.Items.Item("41").Specific;
                oEditF = coForm.Items.Item("31_U_E").Specific;

                if (oCheckBoxF.Checked)
                {
                    oEditF.Value = Global.oCompany.UserName;
                    coForm.Items.Item("btDel").Enabled = false;
                    coForm.Items.Item("btFind").Enabled = false;
                }


                if (!oCheckBoxF.Checked)
                {
                    oEditF.Value = "";
                    coForm.Items.Item("btDel").Enabled = true;
                    coForm.Items.Item("btFind").Enabled = false;
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetApprovalUser. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void EnableCheckButtons()
        {
            try
            {
                SAPbouiCOM.CheckBox oCheckBox, oCheckBox2, oCheckBoxF;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                oCheckBox = coForm.Items.Item("1000002").Specific;
                oCheckBox2 = coForm.Items.Item("41").Specific;
                oCheckBoxF = coForm.Items.Item("1000003").Specific;

                if ((Global.GetProfile() == "MAN" || Global.GetProfile() == "AUT"))
                {
                    if (oCheckBox.Checked && !oCheckBoxF.Checked && !oCheckBox2.Checked)
                    {
                        coForm.Items.Item("1000002").Enabled = false;
                        coForm.Items.Item("41").Enabled = true;
                        coForm.Items.Item("btDel").Enabled = false;
                        coForm.Items.Item("0_U_G").Enabled = false;
                        coForm.Items.Item("1000003").Enabled = false;
                        if (oCheckBox.Checked && !oCheckBoxF.Checked)
                        {
                            if (Global.GetUserAUT2(coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_CardCode", 0)) == Global.oCompany.UserName)
                            {
                                oCheckBoxF.Item.Enabled = true;
                            }
                            else
                            {
                                oCheckBoxF.Item.Enabled = false;
                            }

                        }
                    }

                    else if (oCheckBox2.Checked || oCheckBoxF.Checked)
                    {
                        coForm.Items.Item("1000002").Enabled = false;
                        coForm.Items.Item("41").Enabled = false;
                        coForm.Items.Item("btDel").Enabled = false;
                        coForm.Items.Item("0_U_G").Enabled = false;
                        coForm.Items.Item("1000003").Enabled = false;
                    }
                    else
                    {
                        if (Global.GetUserBP(coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_CardCode", 0)) == "Y")
                        {
                            coForm.Items.Item("1000002").Enabled = true;
                            coForm.Items.Item("41").Enabled = true;
                            coForm.Items.Item("btDel").Enabled = true;
                            coForm.Items.Item("0_U_G").Enabled = true;
                            coForm.Items.Item("1000003").Enabled = false;
                        }
                        else
                        {
                            coForm.Items.Item("1000002").Enabled = false;
                            coForm.Items.Item("41").Enabled = false;
                            coForm.Items.Item("1000003").Enabled = false;
                            coForm.Items.Item("0_U_G").Enabled = true;

                            if (oCheckBox.Checked || oCheckBox2.Checked)
                            {
                                coForm.Items.Item("0_U_G").Enabled = false;
                                coForm.Items.Item("btDel").Enabled = true;
                            }
                        }
                    }
                }
                else
                {
                    coForm.Items.Item("1000002").Enabled = false;
                    coForm.Items.Item("41").Enabled = false;
                    coForm.Items.Item("1000003").Enabled = false;
                    coForm.Items.Item("0_U_G").Enabled = true;
                    if (oCheckBox.Checked || oCheckBox2.Checked)
                    {
                        coForm.Items.Item("0_U_G").Enabled = false;
                        coForm.Items.Item("btDel").Enabled = true;
                    }

                }

                coForm.Items.Item("24_U_E").Enabled = false;
                coForm.Items.Item("btFind").Enabled = false;

            }
            catch (Exception ex)
            {
                Global.SetMessage("EnableCheckButtons. " + ex.Message, Global.MsgType.Error);
            }
        }

        public void OpenCFLDiscounts(string form, string reform, string findtype, string row)
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                string cardcode = coForm.DataSources.DBDataSources.Item("@B1S_OSLD").GetValue("U_CardCode", 0);

                ChooseFrmLst oCFL = new ChooseFrmLst();
                oCFL.createForm(form, reform, findtype, row,cardcode);

            }
            catch (Exception ex)
            {
                Global.SetMessage("" + ex.Message, Global.MsgType.Error);
            }
        }

    }
}
