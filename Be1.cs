﻿using System;
using System.IO;
using System.Windows.Forms;
using SAPbobsCOM;
using SAPbouiCOM;
using Company = SAPbobsCOM.Company;


namespace AdvancedDiscounts
{
    class Be1
    {
        #region Variables

        public CatchingEvents oCatchingEvents;

        #endregion

        #region Connection

        public Be1()
        {
            SetApplication();
            GetDirectory();
            SetFilters();
            SetConnectionContext();
            InitializeEvents();

        }
        public void SetApplication()
        {
            try
            {
                SAPbouiCOM.SboGuiApi SboGuiApi = null;
                string sConnectionString = null;

                SboGuiApi = new SAPbouiCOM.SboGuiApi();
                sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));
                SboGuiApi.Connect(sConnectionString);
                Global.SBOApp = SboGuiApi.GetApplication(-1);
            }
            catch (Exception e)
            {
                MessageBox.Show("SetApplication" + e.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        public void GetDirectory()
        {
            try
            {
                Global.csDirectory = Environment.CurrentDirectory + @"\";
            }
            catch (Exception e)
            {
                MessageBox.Show("GetDirectory: " + e.Message);
            }

        }

        private void SetConnectionContext()
        {
            try
            {
                Global.oCompany = Global.SBOApp.Company.GetDICompany();

            }
            catch (Exception ex)
            {
                MessageBox.Show("SetConnectionContext: " + ex.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        private void SetFilters()
        {

            try
            {
                EventFilter lofilter;
                EventFilters lofilters = new SAPbouiCOM.EventFilters();

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);


                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
                lofilter.AddEx("ADDEF");
                lofilter.AddEx("B1CFL");
                lofilter.AddEx("ADDINL");
                lofilter.AddEx("ADDEXL");
                lofilter.AddEx("ADESD");
                lofilter.AddEx("ADESD1");
                lofilter.AddEx("ADDIS");
                lofilter.AddEx("ADTAX");
                lofilter.AddEx("B1SADMON");
                lofilter.AddEx("B1SADREC");
                lofilter.AddEx("B1SADIMPORT");
                lofilter.AddEx("139");
                lofilter.AddEx("B1SOPTDIS");
                lofilter.AddEx("B1SDISDET");
                lofilter.AddEx("133");
                lofilter.AddEx("B1SOSLD");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);
                lofilter.AddEx("ADDEF");
                lofilter.AddEx("ADESD");
                lofilter.AddEx("B1SADMON");
                lofilter.AddEx("B1SADREC");
                lofilter.AddEx("B1SOSLD");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
                lofilter.AddEx("ADESD");
                lofilter.AddEx("139");
                lofilter.AddEx("B1SOSLD");
                lofilter.AddEx("ADESD1");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
                lofilter.AddEx("ADDEF");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
                lofilter.AddEx("ADDEF");
                lofilter.AddEx("B1CFL");
                lofilter.AddEx("ADDINL");
                lofilter.AddEx("ADDEXL");
                lofilter.AddEx("ADTAX");
                lofilter.AddEx("ADESD");
                lofilter.AddEx("139");
                lofilter.AddEx("B1SOSLD");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
                lofilter.AddEx("ADDEF");
                lofilter.AddEx("B1CFL");
                lofilter.AddEx("ADESD");
                lofilter.AddEx("ADTAX");
                lofilter.AddEx("B1SADMON");
                lofilter.AddEx("B1SADREC");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);
                lofilter.AddEx("ADESD");
                lofilter.AddEx("B1SOSLD");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_VALIDATE);
                lofilter.AddEx("ADDIS");
                lofilter.AddEx("ADTAX");
                lofilter.AddEx("ADDEXL");
                lofilter.AddEx("139");
                lofilter.AddEx("ADDEF");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD);
                lofilter.AddEx("ADESD");
                lofilter.AddEx("B1SOSLD");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
                lofilter.AddEx("ADESD");
                lofilter.AddEx("139");
                lofilter.AddEx("133");
                lofilter.AddEx("B1SOSLD");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
                lofilter.AddEx("120060803");
                lofilter.AddEx("0");
                lofilter.AddEx("120060805");
                lofilter.AddEx("139");
                lofilter.AddEx("133");

                lofilter = lofilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE);
                lofilter.AddEx("ADESD");

                Global.SBOApp.SetFilter(lofilters);

            }
            catch (Exception e)
            {
                Global.SetMessage("SetFilters: " + e.Message, Global.MsgType.Error);
            }

        }

        #endregion

        #region Events

        public void InitializeEvents()
        {
            oCatchingEvents = new CatchingEvents();
            Global.SBOApp.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBOApp_AppEvent);
            Global.SBOApp.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBOApp_FormDataEvent);
            Global.SBOApp.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBOApp_MenuEvent);
            Global.SBOApp.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBOApp_ItemEvent);
           

        }

        private void SBOApp_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {

            switch (EventType)
            {
                case BoAppEventTypes.aet_ShutDown:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_ServerTerminition:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_CompanyChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
                case BoAppEventTypes.aet_LanguageChanged:
                    System.Windows.Forms.Application.Restart();
                    break;
            }
        }


        private void SBOApp_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinnesObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oCatchingEvents.SBOApplication_FormDataEvent(ref BusinnesObjectInfo, out BubbleEvent);
        }


        private void SBOApp_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oCatchingEvents.SBOApplication_MenuEvent(ref pVal, out BubbleEvent);

        }

        private void SBOApp_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            oCatchingEvents.SBOApplication_ItemEvent(FormUID, ref pVal, out BubbleEvent);
        }

        #endregion

    }
}
