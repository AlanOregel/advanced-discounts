﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedDiscounts
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Be1 oBe1 = new Be1();
            CatchingEvents oCatchingEvents = new CatchingEvents();
            System.Windows.Forms.Application.Run();

        }
    }
}
