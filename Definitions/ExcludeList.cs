﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class ExcludeList
    {
        private SAPbouiCOM.Form coForm;

        public void createForm(string form)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);

            coForm.Freeze(false);
        }

        public void AddRow()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oMatrix = coForm.Items.Item("0_U_G").Specific;

                oMatrix.AddRow();
                oMatrix.ClearRowData(oMatrix.RowCount);


            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }

        public void DeleteRow()
        {
            try
            {
                SAPbouiCOM.Matrix oMatrix;
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oMatrix = coForm.Items.Item("0_U_G").Specific;

                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    if (oMatrix.IsRowSelected(i))
                    {
                        oMatrix.DeleteRow(i);
                    }
                }

                oMatrix.FlushToDataSource();
              

                if (coForm.Mode != SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                {
                    coForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }


            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }


        public void SetData()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                SAPbouiCOM.Matrix oMatrix;
                oMatrix = coForm.Items.Item("0_U_G").Specific;
                SAPbouiCOM.EditText oEdit, oEditRef;

                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    oEdit = oMatrix.Columns.Item("C_0_1").Cells.Item(i).Specific;
                    oEditRef = coForm.Items.Item("0_U_E").Specific;
                    oEdit.Value = oEditRef.Value;
                    oEdit = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific;
                    oEdit.Value = (i).ToString();
                }

            }
            catch (Exception ex)
            {
                Global.SetMessage("SetData. " + ex.Message, Global.MsgType.Error);
            }
        }

        public string GetLevel(int row)
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                SAPbouiCOM.Matrix oMatrix;
                SAPbouiCOM.ComboBox oCombo;

                oMatrix = coForm.Items.Item("0_U_G").Specific;
                oCombo = oMatrix.Columns.Item("C_0_2").Cells.Item(row).Specific;
                return oCombo.Value;
            }
            catch (Exception ex)
            {
                Global.SetMessage("GetLevel. " + ex.Message, Global.MsgType.Error);
                return "";
            }
        }

        public bool ValidateSKU(int row)
        {
            try
            {
                SAPbouiCOM.EditText oEdit;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                if (coForm.UniqueID == "ADDEXL")
                {

                    SAPbouiCOM.Matrix oMatrix = coForm.Items.Item("0_U_G").Specific;

                    oEdit = oMatrix.Columns.Item("C_0_3").Cells.Item(row).Specific;
                    oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 63, '{oEdit.Value.ToString()}'");

                    if (oEdit.Value.ToString() != "" && oRecSet.RecordCount == 0)
                    {
                        Global.SetMessage("Ingrese codigo de articulo correcto", Global.MsgType.Warning);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Global.SetMessage("ValidateDiscCode. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }
    }
}
