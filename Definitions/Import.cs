﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedDiscounts
{
    class Import
    {
        private SAPbouiCOM.Form coForm;

        public void createForm(string form)
        {
            Global.CreateForm(form);

            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();
            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.EditText loText;
                SAPbouiCOM.UserDataSource loDS;

                if (coForm.DataSources.UserDataSources.Count == 0)
                {

                    loDS = coForm.DataSources.UserDataSources.Add("dsDis", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtDis").Specific;
                    loText.DataBind.SetBound(true, "", "dsDis");

                    loDS = coForm.DataSources.UserDataSources.Add("dsInc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtInc").Specific;
                    loText.DataBind.SetBound(true, "", "dsInc");

                    loDS = coForm.DataSources.UserDataSources.Add("dsExc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtExc").Specific;
                    loText.DataBind.SetBound(true, "", "dsExc");

                    loDS = coForm.DataSources.UserDataSources.Add("dsTaxAct", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtTaxAct").Specific;
                    loText.DataBind.SetBound(true, "", "dsTaxAct");

                    loDS = coForm.DataSources.UserDataSources.Add("dsDisDef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtDisDef").Specific;
                    loText.DataBind.SetBound(true, "", "dsDisDef");

                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SelectFile(String btnSelect)
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                Thread t = new Thread(() =>
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx|Excel Files 2007-2010|*.xls|All files (*.*)|*.*";

                    DialogResult dr = openFileDialog.ShowDialog(new Form());
                    if (dr == DialogResult.OK)
                    {
                        string fileName = openFileDialog.FileName;
                        switch (btnSelect)
                        {
                            case "btDis":
                                coForm.DataSources.UserDataSources.Item("dsDis").Value = fileName;
                                break;
                            case "btInc":
                                coForm.DataSources.UserDataSources.Item("dsInc").Value = fileName;
                                break;
                            case "btExc":
                                coForm.DataSources.UserDataSources.Item("dsExc").Value = fileName;
                                break;
                            case "btTaxAct":
                                coForm.DataSources.UserDataSources.Item("dsTaxAct").Value = fileName;
                                break;
                            case "btDisDef":
                                coForm.DataSources.UserDataSources.Item("dsDisDef").Value = fileName;
                                break;
                        }

                    }

                });
                t.IsBackground = true;
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
            }
            catch (Exception e)
            {
                Global.SetMessage("SelectFile. " + e.Message, Global.MsgType.Error);
            }

        }

        public void ProcessImportFiles()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                String query, path;

                if (coForm.DataSources.UserDataSources.Item("dsDis").Value != "")
                {
                    query = "SELECT T0.*  FROM [Header$] T0 WHERE T0.Codigo is not null";
                    path = coForm.DataSources.UserDataSources.Item("dsDis").Value;
                    ReadTemplateFile("Discount", path, query);
                }

                if (coForm.DataSources.UserDataSources.Item("dsTaxAct").Value != "")
                {
                    query = "SELECT T0.*  FROM [Header$] T0 WHERE T0.[Codigo Descuento] is not null";
                    path = coForm.DataSources.UserDataSources.Item("dsTaxAct").Value;
                    ReadTemplateFile("AccountTax", path, query);
                }

                if (coForm.DataSources.UserDataSources.Item("dsInc").Value != "")
                {
                    query = "SELECT T0.* FROM [Header$] T0 WHERE T0.[Code] is not null";
                    path = coForm.DataSources.UserDataSources.Item("dsInc").Value;
                    ReadTemplateFile("IncludeList", path, query);
                }

                if (coForm.DataSources.UserDataSources.Item("dsExc").Value != "")
                {
                    query = "SELECT T0.* FROM [Header$] T0 WHERE T0.[Code] is not null";
                    path = coForm.DataSources.UserDataSources.Item("dsExc").Value;
                    ReadTemplateFile("ExcludeList", path, query);
                }

                if (coForm.DataSources.UserDataSources.Item("dsDisDef").Value != "")
                {
                    query = "SELECT T0.* FROM [Descuentos$] T0 WHERE T0.[Id] is not null";
                    path = coForm.DataSources.UserDataSources.Item("dsDisDef").Value;
                    ReadTemplateFile("DiscountDef", path, query);
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("ProcessImportFiles. " + e.Message, Global.MsgType.Error);
            }
        }


        public void ReadTemplateFile(String type, String path, String query)
        {
            OleDbConnection conexion = new OleDbConnection();
            OleDbCommand comando = new OleDbCommand();
            OleDbDataAdapter adaptador = new OleDbDataAdapter();
            DataSet ds = new DataSet();

            try
            {

                conexion.ConnectionString = ($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                conexion.Open();

                comando.CommandText = query;
                comando.Connection = conexion;
                adaptador.SelectCommand = comando;
                adaptador.Fill(ds, "DatosEx");

                switch (type)
                {
                    case "Discount":
                        SaveDiscountCode(ds);
                        break;
                    case "AccountTax":
                        SaveTaxesAndAccounts(ds);
                        break;
                    case "IncludeList":
                        ProcessIncludeExcludeList(type,ds,path);
                        break;
                    case "ExcludeList":
                        ProcessIncludeExcludeList(type, ds, path);
                        break;
                    case "DiscountDef":
                        SaveDiscountDefinition(ds);
                        break;
                }

                conexion.Close();

            }
            catch (Exception e)
            {
                conexion.Close();
                Global.SetMessage("ReadDiscountCodeFile. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SaveDiscountCode(DataSet ds)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 67, '{ds.Tables[0].Rows[i][0].ToString()}'");
                    if (oRecSet.RecordCount > 0)
                    {
                        oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 68,'{ds.Tables[0].Rows[i][0].ToString()}','{ds.Tables[0].Rows[i][1].ToString()}'");
                    }
                    else
                    {
                        oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 43,'{ds.Tables[0].Rows[i][0].ToString()}','{ds.Tables[0].Rows[i][1].ToString()}'");
                    }
                }

                Global.SetMessage("Codigos de descuento actualizados correctamente.", Global.MsgType.Success);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveDiscountCode. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SaveTaxesAndAccounts(DataSet ds)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                String DiscCode, Brand, TaxCode, ActCode;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    DiscCode = ds.Tables[0].Rows[i][0].ToString();
                    Brand = ds.Tables[0].Rows[i][1].ToString();
                    TaxCode = ds.Tables[0].Rows[i][2].ToString();
                    ActCode = ds.Tables[0].Rows[i][3].ToString();

                    oRecSet.DoQuery($"exec B1S_AdvancedDiscounts 69,'{DiscCode}','{Brand}'");
                    if (oRecSet.RecordCount > 0)
                    {
                        oRecSet.DoQuery($"exec B1S_AdvancedDiscounts 68,'{DiscCode}','{Brand}', '{TaxCode}', '{ActCode}'");
                    }
                    else
                    {
                        oRecSet.DoQuery($"exec B1S_AdvancedDiscounts 47,'{DiscCode}','{Brand}', '{TaxCode}', '{ActCode}'");
                    }
                }

                Global.SetMessage("Cuentas e impuesto de descuento actualizados correctamente.", Global.MsgType.Success);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveTaxesAndAccounts. " + e.Message, Global.MsgType.Error);
            }
        }

        private void ProcessIncludeExcludeList(String ListType,DataSet ds, String path)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                OleDbConnection conexion = new OleDbConnection();
                OleDbCommand comando = new OleDbCommand();
                OleDbDataAdapter adaptador = new OleDbDataAdapter();
                DataSet ds2 = new DataSet();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    conexion.ConnectionString = ($"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={path};Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                    conexion.Open();



                    switch (ListType)
                    {
                        case "ExcludeList":
                            comando.CommandText = "SELECT T0.[ItemCode] FROM[Lines$] T0 WHERE T0.[Code] ='" + ds.Tables[0].Rows[i][0].ToString() + "'";
                            comando.Connection = conexion;
                            adaptador.SelectCommand = comando;
                            adaptador.Fill(ds2, "DatosEx");

                            SaveExcludeList(ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][1].ToString(), ds2);
                            break;
                        case "IncludeList":

                            comando.CommandText = "SELECT T0.[Nivel], T0.[Codigo Nivel] FROM[Lines$] T0 WHERE T0.[Code] ='" + ds.Tables[0].Rows[i][0].ToString() + "'";
                            comando.Connection = conexion;
                            adaptador.SelectCommand = comando;
                            adaptador.Fill(ds2, "DatosEx");

                            SaveIncludeList(ds.Tables[0].Rows[i][0].ToString(), ds.Tables[0].Rows[i][1].ToString(), ds2);
                            break;
                    }
                    conexion.Close();
                }

                Global.SetMessage("Lista de inclusion actualizados correctamente.", Global.MsgType.Success);
            }
            catch (Exception e)
            {
                Global.SetMessage("SaveTaxesAndAccounts. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SaveIncludeList(String code, String name,DataSet ds)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                SAPbobsCOM.GeneralService oGeneralService;
                SAPbobsCOM.GeneralData oGeneralData;
                SAPbobsCOM.GeneralDataCollection oSons;
                SAPbobsCOM.GeneralData oSon;
                SAPbobsCOM.CompanyService sCmp;

                sCmp = Global.oCompany.GetCompanyService();

                oGeneralService = sCmp.GetGeneralService("B1S_OINL");
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralData.SetProperty("Code", code);
                oGeneralData.SetProperty("Name", name);

                //'Specify data for child UDO
                oSons = oGeneralData.Child("B1S_INL1");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    oSon = oSons.Add();
                    oSon.SetProperty("U_Level", ds.Tables[0].Rows[i][0].ToString());
                    oSon.SetProperty("U_Id", ds.Tables[0].Rows[i][1].ToString());

                    switch (ds.Tables[0].Rows[i][0].ToString())
                    {
                        case "0":
                            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 70, '" + ds.Tables[0].Rows[i][1].ToString() + "'");
                            break;
                        case "1":
                            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 71, '" + ds.Tables[0].Rows[i][1].ToString() + "'");
                            break;
                        case "2":
                            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 72, '" + ds.Tables[0].Rows[i][1].ToString() + "'");
                            break;
                        case "3":
                            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 73, '" + ds.Tables[0].Rows[i][1].ToString() + "'");
                            break;
                        case "4":
                            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 74, '" + ds.Tables[0].Rows[i][1].ToString() + "'");
                            break;
                        case "5":
                            oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 75, '" + ds.Tables[0].Rows[i][1].ToString() + "'");
                            break;
                    }
                    oSon.SetProperty("U_Dscription", oRecSet.Fields.Item(0).Value.ToString());
                }

                oGeneralService.Add(oGeneralData);                

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveIncludeList. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SaveExcludeList(String code, String name, DataSet ds)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                SAPbobsCOM.GeneralService oGeneralService;
                SAPbobsCOM.GeneralData oGeneralData;
                SAPbobsCOM.GeneralDataCollection oSons;
                SAPbobsCOM.GeneralData oSon;
                SAPbobsCOM.CompanyService sCmp;

                sCmp = Global.oCompany.GetCompanyService();

                oGeneralService = sCmp.GetGeneralService("B1S_OEXL");
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
                oGeneralData.SetProperty("Code", code);
                oGeneralData.SetProperty("Name", name);

                //'Specify data for child UDO
                oSons = oGeneralData.Child("B1S_EXL1");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    oSon = oSons.Add();
                    oSon.SetProperty("U_Level", "5");
                    oSon.SetProperty("U_Id", ds.Tables[0].Rows[i][0].ToString());
                    
                    oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 75, '" + ds.Tables[0].Rows[i][0].ToString() + "'");
                  
                    oSon.SetProperty("U_Dscripti", oRecSet.Fields.Item(0).Value.ToString());
                }

                oGeneralService.Add(oGeneralData);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveExcludeList. " + e.Message, Global.MsgType.Error);
            }
        }

        private void SaveDiscountDefinition(DataSet ds)
        {
            try
            {
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                string id, CardCode, DiscCode, DiscName, DiscType, ValType, DiscCalc, ApplType, IncCode, ExcCode, AddrsCode, AcctCode, Comments, DestDoc;
                double Value, SalesAmnt;
                string stStrtDate, stEndDate, stAppStrtDate, stAppEndDate;
                DateTime sStrtDate, sEndDate, sAppStrtDate, sAppEndDate;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    id = ds.Tables[0].Rows[i][0].ToString();
                    CardCode = ds.Tables[0].Rows[i][1].ToString();
                    ApplType = ds.Tables[0].Rows[i][2].ToString();                    
                    DiscCode = ds.Tables[0].Rows[i][3].ToString();

                    oRecSet.DoQuery($"Select DiscName From B1SADDIS T0 Where T0.DiscCode = '{ds.Tables[0].Rows[i][3].ToString()}'");
                    DiscName = oRecSet.Fields.Item(0).Value.ToString();

                    DiscType = ds.Tables[0].Rows[i][4].ToString(); 
                    ValType = ds.Tables[0].Rows[i][5].ToString();
                    Value = Convert.ToDouble(ds.Tables[0].Rows[i][6].ToString());
                    DiscCalc = ds.Tables[0].Rows[i][7].ToString();
                    IncCode = ds.Tables[0].Rows[i][8].ToString();
                    ExcCode = ds.Tables[0].Rows[i][9].ToString();
          
                    if (ds.Tables[0].Rows[i][10].ToString() != "")
                    {
                        sStrtDate = Convert.ToDateTime(ds.Tables[0].Rows[i][10]);
                        stStrtDate = sStrtDate.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        stStrtDate = "";
                    }

                    if (ds.Tables[0].Rows[i][11].ToString() != "")
                    {
                        sEndDate = Convert.ToDateTime(ds.Tables[0].Rows[i][11]);
                        stEndDate = sEndDate.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        stEndDate = "";
                    }

                    AddrsCode = ds.Tables[0].Rows[i][12].ToString(); 

                   
                    if (ds.Tables[0].Rows[i][13].ToString() != "")
                    {
                        SalesAmnt = Convert.ToDouble(ds.Tables[0].Rows[i][13].ToString());
                    }
                    else
                    {
                        SalesAmnt = 0.00;
                    }

                    AcctCode = "";
                    Comments = ds.Tables[0].Rows[i][14].ToString();                    
                    DestDoc = ds.Tables[0].Rows[i][15].ToString();

                    if (ds.Tables[0].Rows[i][16].ToString() != "")
                    {
                        sAppStrtDate = Convert.ToDateTime(ds.Tables[0].Rows[i][16]);
                        stAppStrtDate = sAppStrtDate.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        stAppStrtDate = "";
                    }

                    if (ds.Tables[0].Rows[i][17].ToString() != "")
                    {
                        sAppEndDate = Convert.ToDateTime(ds.Tables[0].Rows[i][17]);
                        stAppEndDate = sAppEndDate.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        stAppEndDate = "";
                    }

                    if (id == "0")
                    {
                        oRecSet.DoQuery("exec B1S_AdvancedDiscounts 21,'" + CardCode + " ','" + DiscCode + "'," +
                                                                      "'" + DiscName + " ','" + DiscType + "'," +
                                                                      "'" + ValType + " ','" + Value + "'," +
                                                                      "'" + DiscCalc + " ','" + ApplType + "'," +
                                                                      "'" + IncCode + " ','" + ExcCode + "'," +
                                                                      "'" + stStrtDate + " ','" + stEndDate + "'," +
                                                                      "'" + AddrsCode + " ','" + SalesAmnt + "'," +
                                                                      "'" + AcctCode + " ','" + Comments + "'," +
                                                                      "'" + DestDoc + "','Y'," +
                                                                      "'" + stAppStrtDate + " ','" + stAppEndDate + "'"
                        );
                    }
                    else
                    {
                        oRecSet.DoQuery("exec B1S_AdvancedDiscounts 2,'" + CardCode + " ','" + DiscCode + "'," +
                                              "'" + DiscName + " ','" + DiscType + "'," +
                                              "'" + ValType + " ','" + Value + "'," +
                                              "'" + DiscCalc + " ','" + ApplType + "'," +
                                              "'" + IncCode + " ','" + ExcCode + "'," +
                                              "'" + stStrtDate + " ','" + stEndDate + "'," +
                                              "'" + AddrsCode + " ','" + SalesAmnt + "'," +
                                              "'" + AcctCode + " ','" + Comments + "'," +
                                              "'" + DestDoc + "','" + id + "'," +
                                              "'" + stAppStrtDate + " ','" + stAppEndDate + "'");
                    }


                }

                Global.SetMessage("Definicion de descuento actualizados correctamente.", Global.MsgType.Success);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveDiscountCode. " + e.Message, Global.MsgType.Error);
            }
        }
    }
}
