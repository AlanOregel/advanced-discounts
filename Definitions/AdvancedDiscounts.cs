﻿using System;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Collections;
using System.Windows.Forms;

namespace AdvancedDiscounts
{
    class AdvancedDiscounts
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public string stCardCode
        {
            get
            {   coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                return coForm.DataSources.UserDataSources.Item("dsSN").Value;
            }
        }

        public void createForm(string form)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();
            LoadComboTrigger();
            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.EditText loText;
                SAPbouiCOM.UserDataSource loDS;
                SAPbouiCOM.ComboBox loComb;
                SAPbouiCOM.DataTable oDataTable;
                

                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    loDS = coForm.DataSources.UserDataSources.Add("dsSN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtCust").Specific;
                    loText.DataBind.SetBound(true, "", "dsSN");

                    loDS = coForm.DataSources.UserDataSources.Add("dsSNN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loText = coForm.Items.Item("edtCName").Specific;
                    loText.DataBind.SetBound(true, "", "dsSNN");


                    loDS = coForm.DataSources.UserDataSources.Add("dsTrig", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
                    loComb = coForm.Items.Item("edtTrigger").Specific;
                    loComb.DataBind.SetBound(true, "", "dsTrig");


                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;
                  }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }


        public void LoadComboTrigger()
        {
            try
            {

                SAPbouiCOM.ComboBox oCombo;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                oCombo = coForm.Items.Item("edtTrigger").Specific;

                if (oCombo.ValidValues.Count > 0)
                {
                    do
                    {
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                    }
                    while (oCombo.ValidValues.Count > 0);
                }

                oCombo.ValidValues.Add("TP1", "Orden de venta");
                oCombo.ValidValues.Add("TP2", "Inmediato Nota Crédito");
                oCombo.ValidValues.Add("TP3", "Facturas Acumuladas");
                oCombo.ValidValues.Add("TP4", "Opcional");
                oCombo.ValidValues.Add("TP5", "Volumen de  Venta");
                oCombo.ValidValues.Add("ALL", "Todos");

                oCombo.Item.DisplayDesc = true;
                oCombo.Select("ALL", BoSearchKey.psk_ByValue);
            }
            catch (Exception e)
            {
                Global.SetMessage("LoadComboTrigger. " + e.Message, Global.MsgType.Error);
            }
        }

        public void SetDatos(string stValue, string stChose, string stValue2 = "")
        {
            try
            {
                coForm = Global.SBOApp.Forms.Item("ADDEF");

                switch (stChose)
                {
                    case "SN":
                        coForm.DataSources.UserDataSources.Item("dsSN").Value = stValue;
                        coForm.DataSources.UserDataSources.Item("dsSNN").Value = stValue2;
                        break;
                    case "WHS":
                        coForm.DataSources.UserDataSources.Item("dsWHS").Value = stValue;
                        break;
                }
            }
            catch (Exception e)
            {
                Global.SetMessage("SetDatos. " + e.Message, Global.MsgType.Error);
            }
        }


        public void LoadData()
        {
            try
            {
                SAPbouiCOM.GridColumn oColumn;
                SAPbouiCOM.ComboBoxColumn oCGC;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                coForm.Freeze(true);

                oGrid.DataTable.ExecuteQuery("exec B1S_AdvancedDiscounts 20,'" +  coForm.DataSources.UserDataSources.Item("dsSN").Value + "','" + coForm.DataSources.UserDataSources.Item("dsTrig").Value + "'");

                oColumn = oGrid.Columns.Item("Activo");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox;

                oColumn = oGrid.Columns.Item("Tipo");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                oCGC = (SAPbouiCOM.ComboBoxColumn) oGrid.Columns.Item("Tipo");


                oCGC.ValidValues.Add("DC", "DC");
                oCGC.ValidValues.Add("DCP", "DCP");
                oCGC.ValidValues.Add("DCF", "DCF");
                oCGC.ValidValues.Add("DCE", "DCE");
                oCGC.ValidValues.Add("DCMV", "DCMV");
                oCGC.ValidValues.Add("DCPF", "DCPF");
                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                oColumn = oGrid.Columns.Item("Tipo Valor");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                oCGC = (SAPbouiCOM.ComboBoxColumn) oGrid.Columns.Item("Tipo Valor");
                oCGC.ValidValues.Add("I", "Importe");
                oCGC.ValidValues.Add("P", "Porcentaje");

                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;


                oColumn = oGrid.Columns.Item("Calculo");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                oCGC = (SAPbouiCOM.ComboBoxColumn) oGrid.Columns.Item("Calculo");
                oCGC.ValidValues.Add("S", "Subtotal");
                oCGC.ValidValues.Add("C", "Cascada");
                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;


                oColumn = oGrid.Columns.Item("Desencadenador");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Desencadenador");

                oCGC.ValidValues.Add("TP1", "Orden de venta");
                oCGC.ValidValues.Add("TP2", "Inmediato Nota Crédito");
                oCGC.ValidValues.Add("TP3", "Facturas Acumuladas");
                oCGC.ValidValues.Add("TP4", "Opcional");
                oCGC.ValidValues.Add("TP5", "Volumen de  Venta");
                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;


                oColumn = oGrid.Columns.Item("Documento Destino");
                oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Documento Destino");

                oCGC.ValidValues.Add("PO", "Orden de Compra");
                oCGC.ValidValues.Add("CM", "Nota de Credito");
                oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;


                SAPbouiCOM.EditTextColumn oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cuenta");
                oColumns.LinkedObjectType = "1";
      
                oGrid.Columns.Item("Id").Visible = false;
                oGrid.Columns.Item("Cuenta").Visible = false;

                if (oGrid.DataTable.Rows.Count > 0)
                {
                    for (int i = 0; i <= oGrid.DataTable.Rows.Count -1; i++)
                    {
                        DisableColumns(i);
                        DisableColumnsDocType(i);
                        DisableColumnsSubTotal(i);
                    } 
                }

                oGrid.AutoResizeColumns();

                coForm.Freeze(false);


            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage("LoadData. " + e.Message, Global.MsgType.Error);
            }
        }

        public void AddRow()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                oGrid.DataTable.Rows.Add();
            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }

        public void GridRowDelete()
        {
            try
            {
                int iRow, i, iDTRowIndex, iRowTotal;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                System.Collections.Hashtable hTable;
                hTable = new Hashtable();

                iRow = 0;

                for(i=0;i<= oGrid.Rows.Count - 1; i++)
                {
                    iDTRowIndex = oGrid.GetDataTableRowIndex(i);
                    if(iDTRowIndex >= 0)
                    {
                        if (oGrid.Rows.IsSelected(i))
                        {
                            hTable.Add(iRow, "J");
                        }
                        iRow = iRow + 1;
                    }
                }

                iRow = iRow - 1;

                iRowTotal = oGrid.Rows.Count - 1;

                for (i = iRowTotal; i >= 0; i--)
                {
                    iDTRowIndex = oGrid.GetDataTableRowIndex(i);
                    if ((iDTRowIndex >= 0))
                    {
                        if (hTable.ContainsKey(iRow))
                        {
                            oGrid.DataTable.Rows.Remove(iRow);
                        }

                        iRow = (iRow - 1);
                    }

                }

                hTable.Clear();


            }
            catch (Exception e)
            {
                Global.SetMessage("GridRowDelete. " + e.Message, Global.MsgType.Error);
                throw;
            }
        }

        public void DisableColumns(int row)
        {
            SAPbouiCOM.Grid oGrid;
            string type;

            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                type = oGrid.DataTable.GetValue("Tipo", row);
                switch (type)
                {
                    case "DC":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 9, false);
                        // Codigo asignacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 10, false);
                        // Codigo exclusion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 11, false);
                        // Fecha inicio
                        oGrid.CommonSetting.SetCellEditable((row + 1), 12, false);
                        // Fecha Fin
                        oGrid.CommonSetting.SetCellEditable((row + 1), 13, false);
                        // Codigo direccon
                        oGrid.CommonSetting.SetCellEditable((row + 1), 14, false);
                        // Volumen venta
                        oGrid.CommonSetting.SetCellEditable((row + 1), 15, false);
                        //Inicio aplicacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 16, false);
                        //Fin Aplicacion

                        oGrid.CommonSetting.SetCellFontColor(row + 1, 11, 15198183); //Fecha inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 12, 15198183); //Fecha Fin
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 15, 15198183); //Fecha App inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 16, 15198183); //Fecha App Fin

                        break;

                    case "DCP":
                        oGrid.CommonSetting.SetCellEditable((row + 1),9, true);
                        // Codigo asignacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 10, true);
                        // Codigo exclusion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 11, false);
                        // Fecha inicio
                        oGrid.CommonSetting.SetCellEditable((row + 1), 12, false);
                        // Fecha Fin
                        oGrid.CommonSetting.SetCellEditable((row + 1), 13, false);
                        // Codigo direccon
                        oGrid.CommonSetting.SetCellEditable((row + 1), 14, false);
                        // Volumen venta
                        oGrid.CommonSetting.SetCellEditable((row + 1), 15, false);
                        //Inicio aplicacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 16, false);
                        //Fin Aplicacion

                        oGrid.CommonSetting.SetCellFontColor(row + 1, 11, 15198183); //Fecha inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 12, 15198183); //Fecha Fin
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 15, 15198183); //Fecha App inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 16, 15198183); //Fecha App Fin


                        break;

                    case "DCPF":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 9, true);
                        // Codigo asignacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 10, true);
                        // Codigo exclusion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 11, true);
                        // Fecha inicio
                        oGrid.CommonSetting.SetCellEditable((row + 1), 12, true);
                        // Fecha Fin
                        oGrid.CommonSetting.SetCellEditable((row + 1), 13, false);
                        // Codigo direccon
                        oGrid.CommonSetting.SetCellEditable((row + 1), 14, false);
                        // Volumen venta
                        oGrid.CommonSetting.SetCellEditable((row + 1), 15, false);
                        //Inicio aplicacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 16, false);
                        //Fin Aplicacion

                        oGrid.CommonSetting.SetCellFontColor(row + 1, 11, 0); //Fecha inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 12, 0); //Fecha Fin
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 15, 15198183); //Fecha App inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 16, 15198183); //Fecha App Fin

                        break;

                    case "DCE":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 9, false);
                        // Codigo asignacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 10, false);
                        // Codigo exclusion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 11, false);
                        // Fecha inicio
                        oGrid.CommonSetting.SetCellEditable((row + 1), 12, false);
                        // Fecha Fin
                        oGrid.CommonSetting.SetCellEditable((row + 1), 13, true);
                        // Codigo direccon
                        oGrid.CommonSetting.SetCellEditable((row + 1), 14, false);
                        // Volumen venta
                        oGrid.CommonSetting.SetCellEditable((row + 1), 15, false);
                        //Inicio aplicacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 16, false);
                        //Fin Aplicacion

                        oGrid.CommonSetting.SetCellFontColor(row + 1, 11, 15198183); //Fecha inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 12, 15198183); //Fecha Fin
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 15, 15198183); //Fecha App inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 16, 15198183); //Fecha App Fin


                        break;

                    case "DCF":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 9, false);
                        // Codigo asignacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 10, false);
                        // Codigo exclusion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 11, true);
                        // Fecha inicio
                        oGrid.CommonSetting.SetCellEditable((row + 1), 12, true);
                        // Fecha Fin
                        oGrid.CommonSetting.SetCellEditable((row + 1), 13, false);
                        // Codigo direccon
                        oGrid.CommonSetting.SetCellEditable((row + 1), 14, false);
                        // Volumen venta
                        oGrid.CommonSetting.SetCellEditable((row + 1), 15, false);
                        //Inicio aplicacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 16, false);
                        //Fin Aplicacion

                        oGrid.CommonSetting.SetCellFontColor(row + 1, 11, 0); //Fecha inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 12, 0); //Fecha Fin
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 15, 15198183); //Fecha App inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 16, 15198183); //Fecha App Fin


                        break;
                    case "DCMV":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 9, true);
                        // Codigo asignacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 10, true);
                        // Codigo exclusion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 11, true);
                        // Fecha inicio
                        oGrid.CommonSetting.SetCellEditable((row + 1), 12, true);
                        // Fecha Fin
                        oGrid.CommonSetting.SetCellEditable((row + 1), 13, false);
                        // Codigo direccon
                        oGrid.CommonSetting.SetCellEditable((row + 1), 14, true);
                        // Volumen venta
                        oGrid.CommonSetting.SetCellEditable((row + 1), 15, true);
                        //Inicio aplicacion
                        oGrid.CommonSetting.SetCellEditable((row + 1), 16, true);
                        //Fin Aplicacion

                        oGrid.CommonSetting.SetCellFontColor(row + 1, 11, 0); //Fecha inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 12, 0); //Fecha Fin
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 15, 0); //Fecha App inicio
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 16, 0); //Fecha App Fin


                        break;

                }
            }
            catch (Exception e)
            {
                Global.SetMessage("DisableColumns. " + e.Message, Global.MsgType.Error);
            }

        }

        public void DisableColumnsDocType(int row)
        {
            SAPbouiCOM.Grid oGrid;
            string type;

            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                coForm.Freeze(true);
                type = oGrid.DataTable.GetValue("Desencadenador", row);
                switch (type)
                {
                    case "TP5":
                    case "TP3":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 19, true);
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 19, 0);
                        oGrid.CommonSetting.SetCellEditable((row + 1), 6, true);
                        oGrid.CommonSetting.SetCellEditable((row + 1), 8, true);
                        break;

                    case "TP1":


                        oGrid.CommonSetting.SetCellEditable((row + 1), 19, false);
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 19, 15198183);

                        oGrid.CommonSetting.SetCellEditable((row + 1), 6, false);
                        oGrid.CommonSetting.SetCellEditable((row + 1), 8, false);

                        oGrid.DataTable.SetValue("Tipo Valor", row, "P");
                        oGrid.DataTable.SetValue("Calculo", row, "S");

                        break;
                    default:
                        oGrid.CommonSetting.SetCellEditable((row + 1), 6, true);
                        oGrid.CommonSetting.SetCellEditable((row + 1), 8, true);
                        oGrid.CommonSetting.SetCellEditable((row + 1), 19, false);
                        oGrid.CommonSetting.SetCellFontColor(row + 1, 19, 15198183);
                        break;
                }
                coForm.Freeze(false);
            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage("DisableColumns. " + e.Message, Global.MsgType.Error);
            }

        }


        public void DisableColumnsSubTotal(int row)
        {
            SAPbouiCOM.Grid oGrid;
            string type;

            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                coForm.Freeze(true);
                type = oGrid.DataTable.GetValue("Tipo Valor", row);
                switch (type)
                {
                    case "I":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 8, false);
                        oGrid.DataTable.SetValue("Calculo", row, "S");
                        break;

                    case "P":
                        oGrid.CommonSetting.SetCellEditable((row + 1), 8, true);
                        break;
                    default:
                        oGrid.CommonSetting.SetCellEditable((row + 1), 8, false);
                        break;
                }
                coForm.Freeze(false);
            }
            catch (Exception e)
            {
                coForm.Freeze(false);
                Global.SetMessage("DisableColumns. " + e.Message, Global.MsgType.Error);
            }

        }

        public void SaveData()
        {
            try
            {
                string id,CardCode, DiscCode, DiscName, DiscType, ValType, DiscCalc, ApplType, IncCode, ExcCode, AddrsCode, AcctCode, Comments, DestDoc,Active;
                double Value, SalesAmnt;
                string stStrtDate, stEndDate, stAppStrtDate, stAppEndDate;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                DeletepreviousData();

                if (oGrid.DataTable.Rows.Count == 0)
                {
                    return;
                }

                for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                {
                    id = oGrid.DataTable.GetValue("Id", i).ToString();
                    CardCode = coForm.DataSources.UserDataSources.Item("dsSN").Value;
                    DiscCode = oGrid.DataTable.GetValue("Descuento", i);
                    DiscName = oGrid.DataTable.GetValue("Descripcion", i);
                    DiscType = oGrid.DataTable.GetValue("Tipo", i);
                    ValType = oGrid.DataTable.GetValue("Tipo Valor", i);
                    DiscCalc = oGrid.DataTable.GetValue("Calculo", i);
                    ApplType = oGrid.DataTable.GetValue("Desencadenador", i);
                    IncCode = oGrid.DataTable.GetValue("Codigo asignacion", i);
                    ExcCode = oGrid.DataTable.GetValue("Codigo exclusion", i);
                    AddrsCode = oGrid.DataTable.GetValue("Codigo Direccion", i);
                    AcctCode = oGrid.DataTable.GetValue("Cuenta", i);
                    Comments = oGrid.DataTable.GetValue("Comentarios", i);
                    Value = oGrid.DataTable.GetValue("Valor", i);
                    SalesAmnt = oGrid.DataTable.GetValue("Vol. Venta", i);
                    DestDoc = oGrid.DataTable.GetValue("Documento Destino", i);
                    Active = oGrid.DataTable.GetValue("Activo", i);

                    try
                    {
                        stStrtDate = oGrid.DataTable.GetValue("Fecha Inicio", i).ToString("yyyy-MM-dd");
                    }
                    catch (Exception)
                    {
                        stStrtDate = null;
                    }


                    try
                    {
                        stEndDate = oGrid.DataTable.GetValue("Fecha Fin", i).ToString("yyyy-MM-dd");
                    }
                    catch (Exception)
                    {
                        stEndDate = null;
                    }


                    try
                    {
                        stAppStrtDate = oGrid.DataTable.GetValue("Inicio Prd. Aplicacion", i).ToString("yyyy-MM-dd");
                    }
                    catch (Exception)
                    {
                        stAppStrtDate = null;
                    }


                    try
                    {
                        stAppEndDate = oGrid.DataTable.GetValue("Fin Prd. Aplicacion", i).ToString("yyyy-MM-dd");
                    }
                    catch (Exception)
                    {
                        stAppEndDate = null;
                    }


                    if (DiscCode != "" && DiscType != "" && ValType != "" && ApplType != "" && DiscCalc !="" )
                    {
                        oRecSet.DoQuery("exec B1S_AdvancedDiscounts 21,'" + CardCode + " ','" + DiscCode + "'," +
                                                                      "'" + DiscName + " ','" + DiscType + "'," +
                                                                      "'" + ValType + " ','" + Value + "'," +
                                                                      "'" + DiscCalc + " ','" + ApplType + "'," +
                                                                      "'" + IncCode + " ','" + ExcCode + "'," +
                                                                      "'" + stStrtDate + " ','" + stEndDate + "'," +
                                                                      "'" + AddrsCode + " ','" + SalesAmnt + "'," +
                                                                      "'" + AcctCode + " ','" + Comments + "'," +
                                                                      "'" + DestDoc + "','" + Active + "'," +
                                                                      "'" + stAppStrtDate + " ','" + stAppEndDate + "'"

                        );      
                    }
                }

                LoadData();
                Global.SetMessage("Datos guardados", Global.MsgType.Success);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveData. " + e.Message, Global.MsgType.Error);
            }
        }




        public void DeletepreviousData()
        {
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oRecSet.DoQuery("exec B1S_AdvancedDiscounts 22, '" +  coForm.DataSources.UserDataSources.Item("dsSN").Value + "', '" + coForm.DataSources.UserDataSources.Item("dsTrig").Value + "'");
                Global.SetMessage("Borrado Exitoso", Global.MsgType.Success);
            }
            catch (Exception e)
            {
                Global.SetMessage("DeletepreviousData. " + e.Message, Global.MsgType.Error);
            }

        }

        public bool ValidatePercent(int row)
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                if (oGrid.DataTable.GetValue("Tipo Valor", row) == "P")
                {
                    oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 87, '" + coForm.DataSources.UserDataSources.Item("dsSN").Value + "','" + oGrid.DataTable.GetValue("Valor", row) + "'");
                    if (oRecSet.RecordCount > 0)
                    {
                        Global.SetMessage("Alguno de los descuentos supera el limite de descuento", Global.MsgType.Warning);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Global.SetMessage("ValidatePercent. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }
        
        
    }
}
