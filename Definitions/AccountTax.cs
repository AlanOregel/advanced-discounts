﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class AccountTax
    {
        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void createForm(string form)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();
            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.DataTable oDataTable;

                SAPbouiCOM.GridColumn oColumn;
                SAPbouiCOM.ComboBoxColumn oCGC;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();


                if (coForm.DataSources.UserDataSources.Count == 0)
                {

                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 40");

                    oColumn = oGrid.Columns.Item("Marca");
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                    oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Marca");

                    oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 44");
                    oRecSet.MoveFirst();

                    for (int i = 0; i< oRecSet.RecordCount; i++)
                    {
                        oCGC.ValidValues.Add(oRecSet.Fields.Item("Code").Value, oRecSet.Fields.Item("Name").Value);
                        oRecSet.MoveNext();
                    }
                  
                    oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                    oColumn = oGrid.Columns.Item("Tipo Documento");
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                    oCGC = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item("Tipo Documento");
                    oCGC.ValidValues.Add("CM", "Nota de Credito");
                    oCGC.ValidValues.Add("PO", "Orden de Compra");
                    oCGC.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;


                    SAPbouiCOM.EditTextColumn oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Cuenta");
                    oColumns.LinkedObjectType = "1";

                    oColumns = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item("Codigo Impuesto");
                    oColumns.LinkedObjectType = "128";

                    oGrid.AutoResizeColumns();

                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }

        public void AddRow()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                oGrid.DataTable.Rows.Add();
            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }

        public void GridRowDelete()
        {
            try
            {
                int iRow, i, iDTRowIndex, iRowTotal;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                System.Collections.Hashtable hTable;
                hTable = new Hashtable();

                iRow = 0;

                for (i = 0; i <= oGrid.Rows.Count - 1; i++)
                {
                    iDTRowIndex = oGrid.GetDataTableRowIndex(i);
                    if (iDTRowIndex >= 0)
                    {
                        if (oGrid.Rows.IsSelected(i))
                        {
                            hTable.Add(iRow, "J");
                        }
                        iRow = iRow + 1;
                    }
                }

                iRow = iRow - 1;

                iRowTotal = oGrid.Rows.Count - 1;

                for (i = iRowTotal; i >= 0; i--)
                {
                    iDTRowIndex = oGrid.GetDataTableRowIndex(i);
                    if ((iDTRowIndex >= 0))
                    {
                        if (hTable.ContainsKey(iRow))
                        {
                            oGrid.DataTable.Rows.Remove(iRow);
                        }

                        iRow = (iRow - 1);
                    }

                }

                hTable.Clear();
                coForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;

            }
            catch (Exception e)
            {
                Global.SetMessage("GridRowDelete. " + e.Message, Global.MsgType.Error);
                throw;
            }
        }


        public void SaveData()
        {
            try
            {
                string DiscCode, Brand, TaxCode, ActCode,DocType;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                
                if (oGrid.DataTable.Rows.Count == 0)
                {
                    return;
                }

                DeletepreviousData();

                for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                {

                    DiscCode = oGrid.DataTable.GetValue("Codigo Descuento", i);
                    Brand = oGrid.DataTable.GetValue("Marca", i);
                    TaxCode = oGrid.DataTable.GetValue("Codigo Impuesto", i);
                    ActCode = oGrid.DataTable.GetValue("Cuenta", i);
                    DocType = oGrid.DataTable.GetValue("Tipo Documento", i);
                    oRecSet.DoQuery("exec B1S_AdvancedDiscounts 47,'" + DiscCode + " ','" + Brand + "', '" + TaxCode + "', '" + ActCode + "','" + DocType +"'");

                }

                Global.SetMessage("Datos guardados", Global.MsgType.Success);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveData. " + e.Message, Global.MsgType.Error);
            }
        }

        public void DeletepreviousData()
        {
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oRecSet.DoQuery("EXEC B1S_AdvancedDiscounts 48");
                Global.SetMessage("Borrado Exitoso", Global.MsgType.Success);
            }
            catch (Exception e)
            {
                Global.SetMessage("DeletepreviousData. " + e.Message, Global.MsgType.Error);
            }

        }

        public bool ValidateDiscCode(int row, string colUID)
        {
            try
            {
                string ValidateDisc;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);

                if (coForm.UniqueID == "ADTAX")
                {
                    oGrid = coForm.Items.Item("grData").Specific;

                    switch (colUID)
                    {
                        case "Codigo Descuento":
                            ValidateDisc = oGrid.DataTable.GetValue("Codigo Descuento", row);
                            oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 60, '{ValidateDisc}'");
                            break;
                        case "Codigo Impuesto":
                            ValidateDisc = oGrid.DataTable.GetValue("Codigo Impuesto", row);
                            oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 61, '{ValidateDisc}'");
                            break;
                        case "Cuenta":
                            ValidateDisc = oGrid.DataTable.GetValue("Cuenta", row);
                            oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 62, '{ValidateDisc}'");
                            break;

                        default:
                            ValidateDisc = "";
                            break;
                    }              

                    if (ValidateDisc != "")
                    {                      
                        if (oRecSet.RecordCount > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {return true;}
                }
                else { return true; }
            }
            catch (Exception e)
            {
                Global.SetMessage("ValidateDiscCode. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }

        public bool ValidateDuplicated(int row)
        {
            try
            {
                string ValidateDisc, disccode;
                string ValidateBrand, brand;
                string ValidateDocType, doctype;

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);


                if (coForm.UniqueID == "ADTAX")
                {
                    oGrid = coForm.Items.Item("grData").Specific;

                    ValidateDisc = oGrid.DataTable.GetValue("Codigo Descuento", row);
                    ValidateBrand = oGrid.DataTable.GetValue("Marca", row);
                    ValidateDocType = oGrid.DataTable.GetValue("Tipo Documento", row);

                    for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                    {
                        disccode = oGrid.DataTable.GetValue("Codigo Descuento", i);
                        brand = oGrid.DataTable.GetValue("Marca", i);
                        doctype = oGrid.DataTable.GetValue("Tipo Documento", i);

                        if (disccode == ValidateDisc && brand == ValidateBrand && doctype == ValidateDocType && row != i)
                        {
                            Global.SetMessage("Un registro para codigo y marca ya existe, favor ingresa un nuevo código", Global.MsgType.Warning);
                            return false;
                        }

                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Global.SetMessage("ValidateDiscCode. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }
    }
}
