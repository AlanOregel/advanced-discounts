﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedDiscounts
{
    class Discounts
    {

        private SAPbouiCOM.Form coForm;
        private SAPbouiCOM.Grid oGrid;

        public void createForm(string form)
        {
            Global.CreateForm(form);
            coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
            coForm.Freeze(true);
            BindUserDataSources();
            coForm.Freeze(false);
        }

        public void BindUserDataSources()
        {
            try
            {
                SAPbouiCOM.DataTable oDataTable;

                if (coForm.DataSources.UserDataSources.Count == 0)
                {
                    oDataTable = coForm.DataSources.DataTables.Add("Data");
                    oGrid = coForm.Items.Item("grData").Specific;
                    oGrid.DataTable = oDataTable;

                    oGrid.DataTable.ExecuteQuery("EXEC B1S_AdvancedDiscounts 41");
                    oGrid.AutoResizeColumns();
                }

            }
            catch (Exception e)
            {
                Global.SetMessage("BindUserDataSources. " + e.Message, Global.MsgType.Error);
            }

        }

        public void AddRow()
        {
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;
                oGrid.DataTable.Rows.Add();
            }
            catch (Exception e)
            {
                Global.SetMessage("AddRow. " + e.Message, Global.MsgType.Error);
            }
        }

        public void GridRowDelete()
        {
            try
            {
                int iRow, i, iDTRowIndex, iRowTotal;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                System.Collections.Hashtable hTable;
                hTable = new Hashtable();

                iRow = 0;

                for (i = 0; i <= oGrid.Rows.Count - 1; i++)
                {
                    iDTRowIndex = oGrid.GetDataTableRowIndex(i);
                    if (iDTRowIndex >= 0)
                    {
                        if (oGrid.Rows.IsSelected(i))
                        {
                            hTable.Add(iRow, "J");
                        }
                        iRow = iRow + 1;
                    }
                }

                iRow = iRow - 1;

                iRowTotal = oGrid.Rows.Count - 1;

                for (i = iRowTotal; i >= 0; i--)
                {
                    iDTRowIndex = oGrid.GetDataTableRowIndex(i);
                    if ((iDTRowIndex >= 0))
                    {
                        if (hTable.ContainsKey(iRow))
                        {
                            oRecSet.DoQuery($"EXEC B1S_AdvancedDiscounts 59, '{oGrid.DataTable.GetValue("Codigo Descuento", iRow)}'");
                            if (oRecSet.RecordCount > 0)
                            {
                                Global.SetMessage($"No se puede eliminar descuento {oGrid.DataTable.GetValue("Codigo Descuento", iRow)}, ya esta siendo utilizado", Global.MsgType.Warning);
                            }
                            else
                            {
                                oGrid.DataTable.Rows.Remove(iRow);
                            }
                        }

                        iRow = (iRow - 1);
                    }

                }

                hTable.Clear();

                coForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;

            }
            catch (Exception e)
            {
                Global.SetMessage("GridRowDelete. " + e.Message, Global.MsgType.Error);
                throw;
            }
        }


        public void SaveData()
        {
            try
            {
                string DiscCode, DiscName;
                SAPbobsCOM.Recordset oRecSet = Global.Recordset();

                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;


                DeletepreviousData();

                if (oGrid.DataTable.Rows.Count == 0)
                {
                    return;
                }

                for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                {

                    DiscCode = oGrid.DataTable.GetValue("Codigo Descuento", i);
                    DiscName = oGrid.DataTable.GetValue("Descripcion", i);

                    oRecSet.DoQuery("exec B1S_AdvancedDiscounts 43,'" + DiscCode + " ','" + DiscName + "'" );

                }

                Global.SetMessage("Datos guardados", Global.MsgType.Success);

            }
            catch (Exception e)
            {
                Global.SetMessage("SaveData. " + e.Message, Global.MsgType.Error);
            }
        }

        public void DeletepreviousData()
        {
            SAPbobsCOM.Recordset oRecSet = Global.Recordset();
            try
            {
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oRecSet.DoQuery("exec B1S_AdvancedDiscounts 42");
                Global.SetMessage("Borrado Exitoso", Global.MsgType.Success);
            }
            catch (Exception e)
            {
                Global.SetMessage("DeletepreviousData. " + e.Message, Global.MsgType.Error);
            }
        }


        public bool ValidateDiscCode(int row)
        {
            try
            {
                string ValidateDisc, disccode;
               
                coForm = Global.SBOApp.Forms.GetForm(Global.SBOApp.Forms.ActiveForm.TypeEx, Global.SBOApp.Forms.ActiveForm.TypeCount);
                oGrid = coForm.Items.Item("grData").Specific;

                ValidateDisc = oGrid.DataTable.GetValue("Codigo Descuento", row);


                for (int i = 0; (i <= (oGrid.DataTable.Rows.Count - 1)); i++)
                {

                    disccode = oGrid.DataTable.GetValue("Codigo Descuento", i);
                    
                    if(disccode == ValidateDisc  && row != i)
                    {
                        Global.SetMessage("El código ya existe, favor ingresa un nuevo código",Global.MsgType.Warning);
                        return false;
                    }

                }

                return true;
            }
            catch (Exception e)
            {
                Global.SetMessage("ValidateDiscCode. " + e.Message, Global.MsgType.Error);
                return false;
            }
        }

    }
}
